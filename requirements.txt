attrs==19.3.0
colour==0.1.5
cycler==0.10.0
dtreeviz==1.0
graphviz==0.14.1
importlib-metadata==1.7.0
iniconfig==1.0.1
joblib==0.16.0
json5==0.9.5
kiwisolver==1.2.0
matplotlib==3.3.0
more-itertools==8.4.0
numpy==1.19.1
packaging==20.4
pandas==1.1.0
patsy==0.5.1
Pillow==7.2.0
pluggy==0.13.1
py==1.9.0
pyparsing==2.4.7
pytest==6.0.1
python-dateutil==2.8.1
pytz==2020.1
scikit-learn==0.23.2
scipy==1.5.2
six==1.15.0
sklearn==0.0
sktime==0.4.1
statsmodels==0.11.1
threadpoolctl==2.1.0
toml==0.10.1
xgboost==1.1.1
zipp==3.1.0
