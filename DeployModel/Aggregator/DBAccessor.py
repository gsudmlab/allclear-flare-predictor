# This class will contain the required methods for communicating with the DB.

from ConfigReader import pooledconnection


class DBAccesor():

    def __init__(self):
        self._pooledconnection = pooledconnection

    # the different methods from this class which will be called.
    def recordMain(self, MainObj):
        sqlString = """Insert into maintable (HarpNumber,IsActive,StartTime,EndTime) VALUES (%s,%s,%s,%s) ON DUPLICATE KEY UPDATE IsActive=VALUES(IsActive), EndTime=VALUES(EndTime)"""
        main_values = (MainObj.HarpNumber, MainObj.IsActive, MainObj.StartTime, MainObj.EndTime)

        try:
            self._pooledconnection.execute(sqlString, main_values, True)
        except Exception as ex:
            print(ex)

    def recordTracker(self, TrackerObjList):
        sqlString = """Insert into tracker (HarpNumber,ObsStart,BitmapDownloaded,ConfDownloaded,MagnetogramDownloaded) VALUES (%s,%s,%s,%s,%s)"""
        tracker_values = []
        for tracker in TrackerObjList:
            tracker_tuple = (tracker.HarpNumber, tracker.ObsStart, tracker.BitmapDownloaded, tracker.ConfDownloaded,
                             tracker.MagnetogramDownloaded)
            tracker_values.append(tracker_tuple)
        try:
            if (len(tracker_values) > 1):
                self._pooledconnection.executemany(sqlString, tracker_values, True)
            else:
                self._pooledconnection.execute(sqlString, tracker_values[0], True)
        except Exception as ex:
            print(ex)

    def recordCalculatedValues(self, calcvalsobject):
        sqlString = """
        INSERT
        INTO
        `test`.
        `calculatedvalues`
        (`LocalTracker`,
         `HarpNumber`,
         `ObsStart`,
         `USFLUX`,
         `MEANGBZ`,
         `R_VALUE`,
         `FDIM`,
         `B_EFF`)
        VALUES
        (%s, %s, %s, %s, %s, %s, %s, %s);"""
        calculatedvalues = (
        calcvalsobject.LocalTracker, calcvalsobject.HarpNumber, calcvalsobject.Obsstart, calcvalsobject.USFLUX,
        calcvalsobject.MEANGBZ, calcvalsobject.R_VALUE, calcvalsobject.FDIM)
        try:
            self._pooledconnection.execute(sqlString, calculatedvalues, True)
        except Exception as ex:
            print(ex)

    def joinfunction(self):
        sql = """SELECT tracker.*
                FROM tracker 
                LEFT OUTER
                JOIN calculatedvalues
                ON calculatedvalues.LocalTracker = tracker.LocalTracker
                WHERE calculatedvalues.LocalTracker IS NULL
                 """
        list = self._pooledconnection.execute(sql)
        return list
