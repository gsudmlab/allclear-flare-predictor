USE test;

CREATE TABLE `last_prediction` (
  `Issue_time` datetime DEFAULT NULL,
  `P(FL)` double DEFAULT NULL,
  `P(ER)` double DEFAULT NULL,
  `P(4FL)` double DEFAULT NULL,
  `CME_speed_est` double DEFAULT NULL,
  `Quality` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO last_prediction
VALUES( "2008-11-11 13:23:44", 0.22, 0.45, 0.12, 750, 'Valid' );