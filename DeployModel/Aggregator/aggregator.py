import joblib
import numpy as np
import pandas as pd
from datetime import datetime, timedelta
from DeployModel.src.Meta_DSDO_NRT_SOHO import Meta
from DeployModel.Aggregator.DBAccessor import DBAccesor
from DeployModel.src.CME_speed_estimator import CMESpeedEstimator
from DeployModel.src.MetaFlarePredictorXMCQ import MetaFlarePredictorXMCQ
from DeployModel.Aggregator.ConfigReader import pooledconnection, solar_max, solar_max_rate, solar_min_rate, MX_th


class Aggregator:
    def __init__(self):
        self._DBA = DBAccesor()
        self._prediction_time = datetime(2020, 11, 18, 5, 00, 00)#datetime.now()
        self.last_rec = False

        # Fetch a list of nrt harpnums that are available in the NRT AR database
        self.nrt_harpno_list = self.fetch_nrt_harpnums()
        # DataFrame for storing the individual prediction results
        self.results = pd.DataFrame(data=None, index=self.nrt_harpno_list, columns=['P(FL)', 'P(ER)', 'P(4FL)', 'CME_speed_est', 'Quality'])

        # Predict based on different active regions
        for (arno, row) in self.results.iterrows():
            # in each iteration, issue the predictions for each active region
            # and record the individual results to self.results dataframe
            record = self.fetch_ar_mvts(arno)
            # Check the center point of last record
            cent_p = self.center_point(record[-1])

            if cent_p <= -70:
                print('Center point is less than -70 degree')
                print('Issue a random Forecast with probability: ' + str(self.random_pred()))
                self.results.loc[arno] = {'P(FL)': np.nan, 'P(ER)': np.nan, 'P(4FL)': np.nan, 'CME_speed_est': np.nan, 'Quality': 'Random'}

            elif cent_p > 70:
                print('Center point is greater than 70 degree')
                print('Issue from the last prediction')
                # Go to function fetch_last_available_prediction() to check for last available prediction
                # last_rec will return true if there exists a last available prediction
                self.last_valid_prediction = self.fetch_last_available_prediction()
                # If no available 'valid' predictions, returns random prediction:
                # {'P(FL)': np.nan, 'P(ER)': np.nan, 'P(4FL)': np.nan, 'CME_speed_est': np.nan, 'Quality': 'Random'}
                if not self.last_rec:
                    self.results.loc[arno] = self.random_pred()
                else:
                    self.results.loc[arno] = self.last_valid_prediction
            else:
                pred_result = self.check_last_sixty( self.fetch_ar_mvts(arno) )
                self.results.loc[arno] = pred_result

        print(self.results)
        # Predict the final all clear signal
        print('Final All Clear Signal Prediction: ' + str(self.predict_all_clear()))

    def predict_all_clear(self):
        self.results['P(ALLCLEAR_AR)'] = 0
        for row in self.results.iterrows():
            p_fl = row[1]['P(FL)']
            p_er = row[1]['P(ER)']
            p_4fl = row[1]['P(4FL)']
            cmes_est = row[1]['CME_speed_est']

            # Based on calculation
            p_ar_ac = 1 - ( p_er * int(p_fl > float(MX_th)) * int(cmes_est > 750) )
            row[1]['P(ALLCLEAR_AR)'] = p_ar_ac
        
        # Get the joint probability by assuming independence
        allclear_fd_prob = np.prod(self.results['P(ALLCLEAR_AR)'])
        return allclear_fd_prob
                            
    def fetch_ar_mvts(self, arno):
        # selects the last 12 hours of observations from the database and returns them
        # PS: This function can be changed based on the final meta database
        
        #suppose now is dec 12 13:30
        #ar records span from  Dec 11 01:40 AM - dec 11 23:30 PM
        #last 60 records: dec 11 11:30 AM to dec 11 23:30 PM
        #last 12 hours of records : Nothing! --> dec 12T01:30 to dec 12T13:30

        sql = """SELECT *
                FROM `meta_data`
                WHERE `meta_data`.`HarpNumber` = %s AND `meta_data`.`ObsStart` > %s
                ORDER BY `meta_data`.`ObsStart` ASC
                """
        obs_window_start = self._prediction_time - timedelta(hours=12)
        record = pooledconnection.execute(sql, (arno, obs_window_start))
        return record

    def check_last_sixty(self, record):
        if len(record) < 60:
            print('Less than 12 hours record exists')
            print('Return random prediction ' + str(self.random_pred()))
            check_result = {'P(FL)': np.nan, 'P(ER)': np.nan, 'P(4FL)': np.nan, 'CME_speed_est': np.nan, 'Quality': 'Random'}
        else:
            check_result = self.get_ar_prediction_results(record)

        return check_result

    def frame_mvts(self,armvts):
        df = pd.DataFrame(armvts,
                          columns=['LocalTracker', 'HarpNumber', 'ObsStart', 'USFLUX', 'MEANGBZ', 'R_VALUE', 'PIL_LEN',
                                   'FDIM', 'B_EFF', 'LAT_MIN', 'LON_MIN', 'LAT_MAX', 'LON_MAX', 'CRVAL1', 'CRVAL2',
                                   'CRLN_OBS', 'CRLT_OBS', 'HEAD_USFLUX', 'MEANGAM', 'MEANGBT', 'HEAD_MEANGBZ',
                                   'MEANGBH',
                                   'MEANJZD', 'TOTUSJZ', 'MEANALP', 'MEANJZH', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP',
                                   'MEANPOT',
                                   'TOTPOT', 'MEANSHR', 'SHRGT45', 'HEAD_R_VALUE', 'GWILL', 'QUALITY'])
        li = []
        sg_li = []
        USFLUX = df['USFLUX']
        TOTUSJZ = df['TOTUSJZ']
        TOTUSJH = df['TOTUSJH']
        ABSNJZH = df['ABSNJZH']
        SAVNCPP = df['SAVNCPP']
        TOTPOT = df['TOTPOT']

        li.append([USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT])
        sg_li.append([USFLUX])
        frame = pd.DataFrame(li, columns=['USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT'])
        sg_frame = pd.DataFrame(sg_li, columns=['USFLUX'])
        return frame, sg_frame

    def get_ar_prediction_results(self, armvts):
        # this does the entire prediction
        # returns me four prediction results! [P(FL), P(ER), P(4FL), CMES, quality]
        data, USFLUX_data = self.frame_mvts(armvts)
        pred_meta = self.pred_meta_models(data, USFLUX_data)
        pred_erup = self.pred_erup(data)
        pred_meta_XMCQ_proba, pred_meta_XMCQ = self.pred_meta_XMCQ(data, USFLUX_data)
        pred_cme = self.pred_cme_speed(data, USFLUX_data)
        return {'P(FL)': pred_meta[0][1], 'P(ER)': pred_erup[0][1], 'P(4FL)': pred_meta_XMCQ, 'CME_speed_est': pred_cme,
                'Quality': 'Valid'}

    def fetch_nrt_harpnums(self):
        # returns you a list of nrt harpnums that are available in our NRT AR database
        # PS: This function can be changed based on the final meta database
        sql = """SELECT DISTINCT `meta_data`.`HarpNumber`
                FROM `meta_data`
                """

        record = pooledconnection.execute(sql)
        nrt_harpnums = []
        for acno in record:
            nrt_harpnums.append(acno[0])
        return nrt_harpnums

    def fetch_last_available_prediction(self):
        # Go to the Database table 'last_prediction' and fetch the last prediction record
        # PS: This function can be changed based on the final meta database
        sql = """SELECT `last_prediction`.`Issue_time`, `last_prediction`.`P(FL)`, `last_prediction`.`P(ER)`,
                `last_prediction`.`P(4FL)`, `last_prediction`.`CME_speed_est`, `last_prediction`.`Quality`
                FROM `last_prediction`
                ORDER BY `last_prediction`.`Issue_time`"""

        record = pooledconnection.execute(sql)
        if record:
            self.last_rec = True
            record = record[-1]
        else:
            print('There is no last available prediction record')
        return record

    def center_point(self, armvts):
        LON_MIN = armvts[10]
        LON_MAX = armvts[12]
        cent_p = (LON_MIN + LON_MAX) / 2
        return cent_p

    '''
    def select_last_lon(self):
        # Select last longitude information
        sql = """SELECT `calculatedvalues`.`ObsStart`, `calculatedvalues`.`LON_MIN`, `calculatedvalues`.`LON_MAX`
        FROM `calculatedvalues`"""

        record = pooledconnection.execute(sql)[-1]
        ObsStart = record[0]
        LON_MIN = record[1]
        LON_MAX = record[2]
        center_p = (LON_MIN + LON_MAX) / 2
        return ObsStart, center_p

    def select_last_CRLN_CRLT(self):
        sql = """SELECT `calculatedvalues`.`ObsStart`, `calculatedvalues`.`CRVAL1`, `calculatedvalues`.`CRVAL2`,
                `calculatedvalues`.`CRLN_OBS`, `calculatedvalues`.`CRLT_OBS`
        FROM `calculatedvalues`"""

        record = pooledconnection.execute(sql)[-1]
        ObsStart = record[0]
        CRVAL1 = record[1]
        CRVAL2 = record[2]
        CRLN_OBS = record[3]
        CRLT_OBS = record[4]
        return ObsStart, CRVAL1, CRVAL2, CRLN_OBS, CRLT_OBS

    def last_sixty(self):
        sql = """SELECT `calculatedvalues`.`ObsStart`, `calculatedvalues`.`USFLUX`, `calculatedvalues`.`TOTUSJZ`,
                        `calculatedvalues`.`TOTUSJH`, `calculatedvalues`.`ABSNJZH`, `calculatedvalues`.`SAVNCPP`,
                        `calculatedvalues`.`TOTPOT`
                FROM `calculatedvalues`"""

        record = pooledconnection.execute(sql)[-61:-1]
        last_sixty_record = True
        return record, last_sixty_record

    def heliocentric_angle(self, CRVAL1, CRVAL2, CRLN_OBS, CRLT_OBS):
        # Calculate the Stonyhurst Latitude and Longitude:
        longitude = CRVAL1 - CRLN_OBS
        latitude = CRVAL2 - CRLT_OBS
        x = np.array([latitude, longitude])
        y = np.array([0., 0.])
        return self.great_circle_distance(x, y)

    def great_circle_distance(self, x, y):
        # Calculates the great circle distance (central angle) between
        # Point1: [lat, long]
        # Point2: [lat, long]  = [0, 0] if the angle is required wrt disk center
        # On the surface of a sphere with radius r
        # INPUT UNIT: DEGREES
        x = x * np.pi / 180
        y = y * np.pi / 180
        dlong = x[1] - y[1]
        den = np.sin(x[0]) * np.sin(y[0]) + np.cos(x[0]) * np.cos(y[0]) * np.cos(dlong)
        num = (np.cos(y[0]) * np.sin(dlong)) ** 2 + (
                np.cos(x[0]) * np.sin(y[0]) - np.sin(x[0]) * np.cos(y[0]) * np.cos(dlong)) ** 2
        # Calculate the great circle distance:
        sig = np.arctan2(np.sqrt(num), den) * 180 / np.pi
        return sig
        # print("Heliocentric Angle of point at ", x * 180 / np.pi, " [lat,long] is ", sig, "degrees")
    '''

    def random_pred(self):
        # Issue a random prediction based on whether is solar maximum or not
        # solar_max is the instance defined in the config.ini
        # @TODO move the constants to configuration file
        # create two:
        # @SOLAR_MAX_RANDOM_ERUPTION_RATE: 0.0447
        # @SOLAR_MIN_RANDOM_ERUPTION_RATE: 0.0195

        if solar_max:
            random_p = solar_max_rate
        else:
            random_p = solar_min_rate
        return random_p

    def pred_base_models(self, data, USFLUX_data):
        DSDO = joblib.load('../DSDO_TRp1p2p3p4p5.pkl')
        NRT = joblib.load('../NRT_TRp2p3p4p5.pkl')
        SOHO = joblib.load('../SOHO_TRp1p2p3_TE_NSDOp4p5.pkl')
        pred_DSDO = DSDO.predict_proba(data)
        pred_NRT = NRT.predict_proba(data)
        pred_SOHO = SOHO.predict_proba(USFLUX_data)
        return pred_DSDO, pred_NRT, pred_SOHO

    def pred_meta_models(self, data, USFLUX_data):
        DSDO, NRT, SOHO = self.pred_base_models(data, USFLUX_data)
        meta = Meta()
        pred_meta = meta.predict_weighted_proba([DSDO, NRT, SOHO], [0.61, 0.65, 0.34])
        return pred_meta

    def pred_erup(self, data):
        ER = joblib.load('../../Eruption_pred/ER_vs_NEFL&NF_Trainp2p3_Testp4p5.pkl')
        pred_erup = ER.predict_proba(data)
        return pred_erup

    def pred_meta_XMCQ(self, data, USFLUX_data):
        DSDO, NRT, SOHO = self.pred_base_models(data, USFLUX_data)

        path = '../MetaFlarePredictorXMCQ_dep.pkl'
        meta_XMCQ = MetaFlarePredictorXMCQ(path)

        base_learner_out = {'dsdo_0': DSDO[0][0], 'dsdo_1': DSDO[0][1],
                            'soho_0': SOHO[0][0], 'soho_1': SOHO[0][1],
                            'nsdo_0': NRT[0][0], 'nsdo_1': NRT[0][1]}
        xtest = pd.DataFrame(base_learner_out, index=[0])
        pred_proba = pd.DataFrame(meta_XMCQ.predict_proba(xtest), columns=meta_XMCQ.model.classes_)
        return pred_proba, {'X': pred_proba['X'][0], 'M': pred_proba['M'][0], 'C': pred_proba['C'][0], 'Q': pred_proba['Q'][0]}

    def pred_cme_speed(self, data, USFLUX_data):
        path = '../MetaFlarePredictorXMCQ_dep.pkl'
        meta_XMCQ = MetaFlarePredictorXMCQ(path)
        pred_meta_XMCQ_proba, pred_meta_XMCQ = self.pred_meta_XMCQ(data, USFLUX_data)
        proba = pd.DataFrame(pred_meta_XMCQ_proba, columns=meta_XMCQ.model.classes_)[['X', 'M', 'C']].to_dict('records')[0]
        e1, e2, e3 = CMESpeedEstimator(proba).estimate()
        return e1

    '''
    def maincall(self, path):
        #ObsStart, CRVAL1, CRVAL2, CRLN_OBS, CRLT_OBS = self.select_last_lat_lon()
        #helio_angle = self.heliocentric_angle(CRVAL1, CRVAL2, CRLN_OBS, CRLT_OBS)

        #ObsStart, cent_p = self.select_last_lon()
        #print("Last record info:")
        #print(ObsStart, cent_p)

        df = pd.read_csv(path, index_col=None, header=0, sep='\t')
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        LON_MIN = df['LON_MIN'][59]
        LON_MAX = df['LON_MAX'][59]
        cent_p = (LON_MIN + LON_MAX) / 2

        result_df = pd.DataFrame(columns=['P(FL)', 'P(ER)', 'P(4FL)', 'CME_speed_est', 'Quality'])
        if cent_p <= -70:
            print('Center point is less than -70 degree')
            print('Issue a random Forecast with probability: ' + str(self.random_pred()))
            result = {'P(FL)': np.nan, 'P(ER)': np.nan, 'P(4FL)': np.nan, 'CME_speed_est': np.nan, 'Quality': 'Random'}

        elif cent_p > 70:
            print('Center point is greater than 70 degree')
            print('Issue from the last prediction')
            result = {'P(FL)': np.nan, 'P(ER)': np.nan, 'P(4FL)': np.nan, 'CME_speed_est': np.nan, 'Quality': 'LastValid'}

        else:
            # Assume there's enough data in the last 12 hours
            # Naive function for reading last 12 hours record
            #record_lst, last_sixty_record = self.last_sixty()

            last_sixty_record = True
            record_lst = df
            if last_sixty_record:
                record = pd.DataFrame(record_lst, columns=['ObsStart', 'USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT'])
                records = record.drop(columns='ObsStart')

                li = []
                sg_li = []
                USFLUX = records['USFLUX']
                TOTUSJZ = records['TOTUSJZ']
                TOTUSJH = records['TOTUSJH']
                ABSNJZH = records['ABSNJZH']
                SAVNCPP = records['SAVNCPP']
                TOTPOT = records['TOTPOT']

                li.append([USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT])
                sg_li.append([USFLUX])
                frame = pd.DataFrame(li, columns=['USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT'])
                sg_frame = pd.DataFrame(sg_li, columns=['USFLUX'])
                #self.pred_meta_models(frame, sg_frame)
                result = self.prediction(frame, sg_frame)
                self.last_rec = result
                #self.last_pred()

            else:
                print('There is no enough data')
                print('Issue a random Forecast with probability: ' + str(self.random_pred()))
                result = {'P(FL)': np.nan, 'P(ER)': np.nan, 'P(4FL)': np.nan, 'CME_speed_est': np.nan, 'Quality': 'Random'}

        result_df = result_df.append(result, ignore_index=True)
        print(result_df)
    '''


def main():
    Aggregator()


if __name__ == '__main__':
    main()
