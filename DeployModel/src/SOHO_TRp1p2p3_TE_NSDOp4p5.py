import glob
import joblib
import numpy as np
import pandas as pd
from sklearn.metrics import confusion_matrix
from sktime.classification.compose import TimeSeriesForestClassifier


class SOHO_TSF:
    def __init__(self):
        self.clf = TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced', n_jobs=3)
        pass

    # This method extract the necessary features from the destination folder
    def data_reader(self, loadPath, partitions, flare_label):
        # Read files from the define path
        all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
        count = len(all_files)

        li = []
        for filename in all_files:
            # Read the file and extract necessary features
            df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
            df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
            # Extract label info
            INFO = filename.split('/')[-1]
            # Define Label value based on the file name
            if flare_label == 'NF':
                LABEL = 'CBN'
            else:
                LABEL = 'XM'

            USFLUX = df['USFLUX']

            li.append([USFLUX, LABEL, INFO])

        # Create and return the dataframe build on the extracted features
        partition_frame = pd.DataFrame(li, columns=['USFLUX', 'LABEL', 'INFO'])
        return partition_frame, count

    # This function converts multiple labels into binary labels
    # by defining X, M as flaring class while C, B, N as non-flaring class
    def convert_to_binary_class(self, label_col):
        for i in range(len(label_col)):
            if (label_col[i].startswith('X')) or (label_col[i].startswith('M')):
                label_col[i] = 'XM'
            else:
                label_col[i] = 'CBN'
        return label_col

    # This function build a Time Series Forest Classifier and generate necessary measurements on the classification results
    def fit_pred(self, trainSet, testSet, testSetName, count_list):
        # Append all the data in the training list into one single dataframe
        trainDF = pd.DataFrame([])
        testDF = pd.DataFrame([])
        for i in range(0, len(trainSet)):
            trainDF = trainDF.append(trainSet[i])
        trainDF = trainDF.reset_index(drop=True)

        for j in range(0, len(testSet)):
            testDF = testDF.append(testSet[j])
        testDF = testDF.reset_index(drop=True)

        # Extract training data/labels and testing data/labels
        train_info = trainDF.pop('INFO')
        test_info = testDF.pop('INFO')
        trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
        trainDF_labels = trainDF['LABEL']
        testDF_data = testDF.loc[:, testDF.columns != 'LABEL']
        testDF_labels = testDF['LABEL']

        result_DF = pd.DataFrame(columns=['# Model: TSF', '', '', ''])
        result_DF.loc[0] = [
            "# Model Parameters: **TimeSeriesForestClassifier(class_weight='balanced';criterion='gini';max_depth=3;n_estimators=50;n_jobs=3)**",
            '', '', '']
        result_DF.loc[1] = [
            "# TrainingSet: **/data/ARIAP_partitioned/SOHOpartitions/instances_O12L0P24C6/SOHOp1 & SOHOp2 & SOHO p3**",
            "number of positives(XM): " + str(count_list[1]), "number of negatives(CBN): " + str(count_list[0]), '']
        result_DF.loc[2] = [
            "# TestSet: **/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/instances_O12L0P24/nrt_p4 & nrt_p5**",
            "number of positives(XM): " + str(count_list[3]), "number of negatives(CBN): " + str(count_list[2]), '']
        result_DF.loc[3] = ["# ModelPath: **/home/aji1/FP/FP_SOHO**", '', '', '']

        self.clf.fit(trainDF_data, trainDF_labels)

        # Save the training model
        joblib.dump(self.clf, 'Depl_TRp1p2p3_' + testSetName + '.pkl')
        # Predict label based on testing data
        pred_labels = self.clf.predict(testDF_data)
        pred_proba = self.clf.predict_proba(testDF_data)
        np.save('Depl_TRp1p2p3_' + testSetName + '.npy', pred_proba)
        np.save('Depl_TRp1p2p3_' + testSetName + '_labels.npy', test_info)
        # Convert multiple labels into binary labels
        testDF_labels = self.convert_to_binary_class(testDF_labels)
        pred_labels = self.convert_to_binary_class(pred_labels)
        # Generate a confusion matrix based on given and predicted labels
        scores = confusion_matrix(testDF_labels, pred_labels, labels=["CBN", "XM"]).ravel()
        tn, fp, fn, tp = scores
        result_DF.loc[4] = ["# Confusion Matrix", '', '', '']
        result_DF.loc[5] = ["#", '', 'Predicted', '']
        result_DF.loc[6] = ["#", '', 'XM', 'CBN']
        result_DF.loc[7] = ["# Actual", 'XM', str(tp), str(fn)]
        result_DF.loc[8] = ["# ", 'CBN', str(fp), str(tn)]
        result_DF.loc[9] = ["# ", '', '', '']
        result_DF.loc[10] = ["Metric", 'Score', '', '']

        # TSS
        tss = self.TSS(scores)
        result_DF.loc[11] = ["TSS", str(tss), '', '']

        # HSS2 Definition 2
        hss2 = self.HSS2(scores)
        result_DF.loc[12] = ["HSS2", str(hss2), '', '']

        # GSS
        gss = self.GSS(scores)
        result_DF.loc[13] = ["GSS", str(gss), '', '']

        # Precision Positive
        posPrecision = self.precisionPos(scores)
        result_DF.loc[14] = ["Precision(XM)", str(posPrecision), '', '']

        # TPR
        tpr = self.TPR(scores)
        result_DF.loc[15] = ["TPR/Recall(XM)", str(tpr), '', '']

        # Precision Negative
        negPrecision = self.precisionNeg(scores)
        result_DF.loc[16] = ["Precision(CBN)", str(negPrecision), '', '']

        # TNR
        tnr = self.TNR(scores)
        result_DF.loc[17] = ["TNR/Recall(CBN)", str(tnr), '', '']

        # FAR
        far = self.FAR(scores)
        result_DF.loc[18] = ["FAR/FPR", str(far), '', '']

        # POFD
        pofd = self.POFD(scores)
        result_DF.loc[19] = ["POFD/FDR", str(pofd), '', '']

        # F1(XM)
        f1XM = self.F1Pos(scores)
        result_DF.loc[20] = ["F1(XM)", str(f1XM), '', '']

        # F1(CBN)
        f1CBN = self.F1Neg(scores)
        result_DF.loc[21] = ["F1(CBN)", str(f1CBN), '', '']

        # Return the result measurement dataframe
        return result_DF

    def TSS(self, table):
        """
        Calculates the true skill score based on the true classes and the predicted ones.

        Note: Keep in mind that the order of the class labels 'labels' in
        'confusion_matrix' defines the positive and negative classes. Here we set it to
        ["CBN", "XM"].

        (From Bobra's paper) - The flaring ARs correctly predicted as flaring are called true
        positives (TP), the flaring ARs incorrectly predicted as non-flaring are false negatives (FN),
        the non-flaring ARs correctly predicted as non-flaring are true negatives (TN), and the
        non-flaring ARs incorrectly predicted as flaring are false positives (FP). From these four
        quantities, various metrics are computed.
        """
        # this order is in line with the confusion_matrix function we use here.
        TN, FP, FN, TP = table

        tp_rate = TP / float(TP + FN) if TP > 0 else 0  # also known as "recall"
        fp_rate = FP / float(FP + TN) if FP > 0 else 0
        return tp_rate - fp_rate

    def HSS1(self, table):
        """
            Calculates the Heidke skill score based on the output of the confusion table function
            This is the first way to compute - using the Barnes & Leka (2008) definition
        """
        TN, FP, FN, TP = table
        N = TN + FP
        P = TP + FN
        HSS = (TP + TN - N) / float(P)
        return HSS

    def HSS2(self, table):
        """
            Calculates the Heidke skill score based on the output of the confusion table function
            This is the second way to compute - using the Mason & Hoeksema (2010) definition
        """
        TN, FP, FN, TP = table
        N = TN + FP
        P = TP + FN
        HSS = (2 * (TP * TN - FN * FP)) / float((P * (FN + TN) + (TP + FP) * N))
        return HSS

    def GSS(self, table):
        # this order is in line with the confusion_matrix function we use here.
        TN, FP, FN, TP = table

        CH = ((TP + FP) * (TP + FN)) / (TP + FP + FN + TN)
        GSS = (TP - CH) / (TP + FP + FN - CH)
        return GSS

    def precisionPos(self, table):
        TN, FP, FN, TP = table
        precisionPos = TP / float(TP + FP)
        return precisionPos

    def TPR(self, table):
        TN, FP, FN, TP = table
        TPR = TP / float(TP + FN)
        return TPR

    def precisionNeg(self, table):
        TN, FP, FN, TP = table
        precisionNeg = TN / float(TN + FN)
        return precisionNeg

    def TNR(self, table):
        TN, FP, FN, TP = table
        TNR = TN / float(TN + FP)
        return TNR

    def FAR(self, table):
        TN, FP, FN, TP = table
        FAR = FP / float(TP + FP)
        return FAR

    def POFD(self, table):
        TN, FP, FN, TP = table
        POFD = FP / float(TN + FP)
        return POFD

    def F1Pos(self, table):
        TN, FP, FN, TP = table
        precision = TP / float(TP + FP)
        recall = TP / float(TP + FN)
        f1 = 2 * ((precision * recall) / (precision + recall))
        return f1

    def F1Neg(self, table):
        TN, FP, FN, TP = table
        precision = TN / float(TN + FN)
        recall = TN / float(TN + FP)
        f1 = 2 * ((precision * recall) / (precision + recall))
        return f1


def main():
    basepath = '/data/ARIAP_partitioned/SOHOpartitions/instances_O12L0P24C6/'
    testpath = '/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/instances_O12L0P24/'

    tsf = SOHO_TSF()

    # Data reading with binary labels
    partition1_nf, train_nf_p1 = tsf.data_reader(basepath, 'SOHOp1', 'NF')
    partition1_fl, train_fl_p1 = tsf.data_reader(basepath, 'SOHOp1', 'FL')
    partition1 = partition1_nf.append(partition1_fl, ignore_index=True)

    partition2_nf, train_nf_p2 = tsf.data_reader(basepath, 'SOHOp2', 'NF')
    partition2_fl, train_fl_p2 = tsf.data_reader(basepath, 'SOHOp2', 'FL')
    partition2 = partition2_nf.append(partition2_fl, ignore_index=True)

    partition3_nf, train_nf_p3 = tsf.data_reader(basepath, 'SOHOp3', 'NF')
    partition3_fl, train_fl_p3 = tsf.data_reader(basepath, 'SOHOp3', 'FL')
    partition3 = partition3_nf.append(partition3_fl, ignore_index=True)

    partition4_nf, test_nf_p4 = tsf.data_reader(testpath, 'nrt_p4', 'NF')
    partition4_fl, test_fl_p4 = tsf.data_reader(testpath, 'nrt_p4', 'FL')
    partition4 = partition4_nf.append(partition4_fl, ignore_index=True)

    partition5_nf, test_nf_p5 = tsf.data_reader(testpath, 'nrt_p5', 'NF')
    partition5_fl, test_fl_p5 = tsf.data_reader(testpath, 'nrt_p5', 'FL')
    partition5 = partition5_nf.append(partition5_fl, ignore_index=True)

    train_nf = train_nf_p1 + train_nf_p2 + train_nf_p3
    train_fl = train_fl_p1 + train_fl_p2 + train_fl_p3
    test_nf = test_nf_p4 + test_nf_p5
    test_fl = test_fl_p4 + test_fl_p5

    count = [train_nf, train_fl, test_nf, test_fl]
    trainSet = [partition1, partition2, partition3]
    testSet = [partition4, partition5]
    result = tsf.fit_pred(trainSet, testSet, 'TE_NSDOp4p5', count)
    result.to_csv("/home/aji1/FP/test_class/SOHO_result.csv", index=False)


if __name__ == '__main__':
    main()

# pip install sktime


# pip install pandas


# pip install numpy




