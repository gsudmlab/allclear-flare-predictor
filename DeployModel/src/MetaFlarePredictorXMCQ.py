import joblib
import pandas as pd

class MetaFlarePredictorXMCQ:
    def __init__(self, model_path):
        self.model = joblib.load(model_path)
    
    def predict(self, X):
        return self.model.predict(X)
    
    def predict_proba(self, X):
        return self.model.predict_proba(X)
        
### mini-test ###
#path = '../MetaFlarePredictorXMCQ_dep.pkl'
#DSDO = joblib.load('../DSDO_TRp1p2p3p4p5.pkl')
#NRT = joblib.load('../NRT_TRp2p3p4p5.pkl')
#SOHO = joblib.load('../SOHO_TRp1p2p3_TE_NSDOp4p5.pkl')

#loaded_model = MetaFlarePredictorXMCQ(path)
#base_learner_out = {'dsdo_0': 0.99028421232199, 'dsdo_1': 0.009715787678009888,
#          'soho_0': 0.8809621506765992, 'soho_1': 0.1190378493234007,
#          'nsdo_0': 0.5584089316988012, 'nsdo_1': 0.44159106830119893}
#xtest = pd.DataFrame(base_learner_out, index=[0])

#print(xtest)
#Y_pred = loaded_model.predict(xtest)
#Y_proba = pd.DataFrame(loaded_model.predict_proba(xtest), columns=loaded_model.model.classes_)
#print(Y_pred, Y_proba)

#from DeployModel.src.CME_speed_estimator import CMESpeedEstimator
#proba = pd.DataFrame(Y_proba, columns=loaded_model.model.classes_)[['X', 'M', 'C']].to_dict('records')[0]
#CMESpeedEstimator(proba).print_report()
