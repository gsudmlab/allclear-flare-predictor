import joblib
import numpy as np

class Meta:
    def __init__(self):
        pass

    def load_models_result(self, path):
        """Generate a library of base learners."""
        # Load the base model
        model = np.load(path)

        return model

    def predict_weighted(self, pred_results, W):
        result_sum = self.predict_weighted_proba(pred_results, W)
        results = np.argmax(result_sum, axis=1)
        return results

    def predict_weighted_proba(self, pred_results, W):
        if len(pred_results) != len(W):
            print('The size of the weights should match the size of the prediction labels')
        else:
            pred_proba = np.copy(pred_results)
            for i in range(len(W)):
                pred_proba[i] = pred_results[i] * W[i]

            result_sum = np.sum(pred_proba, axis=0) / np.sum(W)
            return result_sum


def main():
    meta = Meta()
    #DSDO = meta.load_models('/Users/annie/Desktop/NRT/FP/Experiments/FP_DSDO/Deploy_TRp1p2p3p4p5.pkl')
    #NRT = meta.load_models('/Users/annie/Desktop/NRT/FP/Experiments/FP_NRT/Deploy_TRp2p3p4p5.pkl')
    #SOHO = meta.load_models('/Users/annie/Desktop/NRT/FP/Experiments/FP_SOHO/Depl_TRp1p2p3_TE_NSDOp4p5.pkl')

    DSDO = meta.load_models_result('/Users/annie/Desktop/NRT/FP/Experiments/FP_DSDO/Exp3_TRp2p3_TE_NSDOp4p5.npy')
    NRT = meta.load_models_result('/Users/annie/Desktop/NRT/FP/Experiments/FP_NRT/NRT_KW_TRp2p3_TEp4p5.npy')
    SOHO = meta.load_models_result('/Users/annie/Desktop/NRT/FP/Experiments/FP_SOHO/Depl_TRp1p2p3_TE_NSDOp4p5.npy')

    result = meta.predict_weighted_proba([DSDO, NRT, SOHO], [0.61, 0.65, 0.34])
    np.save('/Users/annie/PycharmProjects/allclear-flare-predictor/DeployModel/pred_result_exp/META_pred_results.npy', result)
    print(result)

if __name__ == '__main__':
    main()