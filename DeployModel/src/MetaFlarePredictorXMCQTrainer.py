import joblib
import numpy as np
import pandas as pd
from sklearn.ensemble import RandomForestClassifier

class MetaFlarePredictorXMCQTrainer:
    def __init__(self, model_path):
        self.model_path = model_path
        self.model = self.train_model()
    
    def train_model(self):
        data = self.prepare_data()
        X_train = data[['dsdo_0', 'dsdo_1', 'soho_0', 'soho_1', 'nsdo_0', 'nsdo_1']]
        Y_train = data['gclass']
        
        rf = RandomForestClassifier(max_depth=5, n_estimators=250, class_weight='balanced')
        rf.fit(X_train, Y_train)
        
        joblib.dump(rf, self.model_path + '.pkl')

    def goes_class_to_peak_flux(self, gclass):
        if gclass == 'FQ':
            return 1 * 10 ** -8
        category = gclass[0]
        factor = float(gclass[1:4])
        if category == 'A':
            return factor * 10 ** -8
        elif category == 'B':
            return factor * 10 ** -7
        elif category == 'C':
            return factor * 10 ** -6
        elif category == 'M':
            return factor * 10 ** -5
        elif category == 'X':
            return factor * 10 ** -4
        else:
            return np.nan

    def goes_class_lognormalized(self, gclass):
        if gclass == 'FQ':
            return 0
        category = gclass[0]
        if category == 'A':
            return 0
        elif category == 'B':
            return 1
        elif category == 'C':
            return 2
        elif category == 'M':
            return 3
        elif category == 'X':
            return 4
        else:
            return 0

    def goes_class_category(self, gclass):
        if gclass == 'FQ':
            return 'Q'
        return gclass[0]
                
    
    def prepare_data(self): # local, requires data from NRT, NSDO and SOHO models
        labels = np.load('../pred_result_exp/NRT_KW_TRp2p3_TEp4p5_labels.npy', allow_pickle=True)
        proba = np.load('../pred_result_exp/NRT_KW_TRp2p3_TEp4p5.npy', allow_pickle=True)
        nsdo = pd.DataFrame(data=proba, index=labels, columns=['nsdo_0', 'nsdo_1'])

        labels = np.load('../pred_result_exp/Exp3_TRp2p3_TE_NSDOp4p5_labels.npy', allow_pickle = True)
        proba = np.load('../pred_result_exp/Exp3_TRp2p3_TE_NSDOp4p5.npy', allow_pickle = True)

        dsdo = pd.DataFrame(data=proba, index=labels, columns=['dsdo_0', 'dsdo_1'])

        labels = np.load('../pred_result_exp/Depl_TRp1p2p3_TE_NSDOp4p5_labels.npy', allow_pickle = True)
        proba = np.load('../pred_result_exp/SOHO_TRp1p2p3_TE_NSDOp4p5.npy', allow_pickle = True)

        soho_fp = pd.DataFrame(data=proba, index=labels, columns=['soho_0', 'soho_1'])

        results = dsdo.join(soho_fp).join(nsdo)
        results['goes_class'] = [label.split('_')[0].split('@')[0] for label in labels]
        results['gclass'] = results['goes_class'].apply(self.goes_class_category)

        harpnums = []
        cme_vel = []
        cme_width = []
        cme_conf = []

        split_l = [label.split('_') for label in labels]

        for larr in split_l:
            cme_v = np.nan
            cme_w = np.nan
            cme_c = np.nan

            if len(larr) == 4: # FQ
                arno = int(larr[1][2:])
            elif len(larr) == 6: # NE-FL
                arno = int(larr[3][2:])
            elif len(larr) == 11: # ER-FL
                arno = int(larr[8][2:])
                cme_v = float(larr[5][2:])
                cme_w = float(larr[6][2:])
                cme_c = float(larr[7].split(':')[1])

            harpnums.append(arno)
            cme_vel.append(cme_v)
            cme_width.append(cme_w)
            cme_conf.append(cme_c)

        results['cme_confidence'] = cme_conf
        results['cme_velocity'] = cme_vel
        results['cme_width'] = cme_width
        results['harpnum'] = harpnums
        return results
    
        
result = MetaFlarePredictorXMCQTrainer('MetaFlarePredictorXMCQ_test1')
print(result)

