import pandas as pd
import numpy as np
#@final
SC23_FLARE_ESTIMATES = {'X': 1.7e-04, 'M': 1.7e-05, 'C': 2.3e-06}
SC24_FLARE_ESTIMATES = {'X': 1.8e-04, 'M': 1.6e-05, 'C': 1.9e-06}
#sc23w = 0.349 # R2 score for predictor
#sc24w = 0.223 # R2 score for predictor
#sc24 cme estimator: CME Velocity* = 57.78xlog(PXF)^3 + 1041.45xlog(PXF)^2 + 6266.93xlog(PXF)^1 + 13119.68
#sc23 cme estimator: CME Velocity* = -37.61xlog(PXF)^3 + -370.55xlog(PXF)^2 + -543.55xlog(PXF)^1 + 2535.60
sc23coef = [2535.60, -543.55, -370.55, -37.61]
sc24coef = [13119.68, 6266.93, 1041.45, 57.78]

class CMESpeedEstimator:
    def __init__(self, fl_prob, sc23w=0.349,
                                sc23coef=[2535.60, -543.55, -370.55, -37.61],
                                sc24w=0.223,
                                sc24coef=[13119.68, 6266.93, 1041.45, 57.78]):
        self.fl_prob = fl_prob
        self.sc23w_norm = sc23w / (sc23w + sc24w)
        self.sc24w_norm = sc24w / (sc23w + sc24w)
        self.sc23coef = sc23coef
        self.sc24coef = sc24coef

    def estimate(self):
        speed_estimate23 = 0
        speed_estimate24 = 0
        likelihood_sum = 0
        for flclass, p_fl in self.fl_prob.items():
            #print(flclass, p_fl)
            estimated_pxrf_23 = SC23_FLARE_ESTIMATES[flclass]
            estimated_pxrf_24 = SC24_FLARE_ESTIMATES[flclass]
            lpxrf23 = np.log10(estimated_pxrf_23)
            lpxrf24 = np.log10(estimated_pxrf_24)
            speed_estimate23 += p_fl * self.estimate_sc23(lpxrf23)
            speed_estimate24 += p_fl * self.estimate_sc24(lpxrf24)
            likelihood_sum += p_fl
        sc23_cme_speed = speed_estimate23 / likelihood_sum
        sc24_cme_speed = speed_estimate24 / likelihood_sum
        return (self.sc23w_norm * sc23_cme_speed + self.sc24w_norm * sc24_cme_speed), sc23_cme_speed, sc24_cme_speed

    def estimate_sc23(self, lpxrf):
        return np.sum([(coef * (lpxrf**i)) for i, coef in enumerate(self.sc23coef)])

    def estimate_sc24(self, lpxrf):
        return np.sum([(coef * (lpxrf**i)) for i, coef in enumerate(self.sc24coef)])

    def print_report(self):
        e1, e2, e3 = self.estimate()
        if sum(self.fl_prob.values()) < 0.5:
            print('Disclaimer: Likely flare quiet region. ')
        print('Estimated CME speed: {:.2f} km/s # Weighted CME speed estimate of SC23 and SC24 Models\nSC23 Estimate:{:.2f}; SC24 Estimate:{:.2f}'.format(e1, e2, e3))

# USAGE
#CMESpeedEstimator({'X': 0.70, 'M': 0.30, 'C': 0.00}).print_report()



