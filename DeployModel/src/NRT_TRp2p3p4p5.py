import glob
import joblib
import pandas as pd
from sktime.classification.compose import ColumnEnsembleClassifier
from sktime.classification.compose import TimeSeriesForestClassifier


class NRT_TSF:
    def __init__(self):
        self.clf = ColumnEnsembleClassifier(estimators=[
            ("TSF0",
             TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                        n_jobs=6), [0]),
            ("TSF1",
             TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                        n_jobs=6), [1]),
            ("TSF2",
             TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                        n_jobs=6), [2]),
            ("TSF3",
             TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                        n_jobs=6), [3]),
            ("TSF4",
             TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                        n_jobs=6), [4]),
            ("TSF5",
             TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                        n_jobs=6), [5]),
        ])
        pass

    # This method extract the necessary features from the destination folder
    def data_reader(self, loadPath, partitions, flare_label):
        # Read files from the define path
        all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
        count = len(all_files)

        li = []
        for filename in all_files:
            # Read the file and extract necessary features
            df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
            df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
            # Extract label info
            INFO = filename.split('/')[-1]
            # Define Label value based on the file name
            if flare_label == 'NF':
                LABEL = 'CBN'
            else:
                LABEL = 'XM'

            USFLUX = df['USFLUX']
            TOTUSJZ = df['TOTUSJZ']
            TOTUSJH = df['TOTUSJH']
            ABSNJZH = df['ABSNJZH']
            SAVNCPP = df['SAVNCPP']
            TOTPOT = df['TOTPOT']

            li.append([USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT, LABEL])
            # Create and return the dataframe build on the extracted features
        partition_frame = pd.DataFrame(li, columns=['USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT',
                                                    'LABEL'])
        return partition_frame, count

    # This function converts multiple labels into binary labels
    # by defining X, M as flaring class while C, B, N as non-flaring class
    def convert_to_binary_class(self, label_col):
        for i in range(len(label_col)):
            if (label_col[i].startswith('X')) or (label_col[i].startswith('M')):
                label_col[i] = 'XM'
            else:
                label_col[i] = 'CBN'
        return label_col

    # This function build a Time Series Forest Classifier and generate necessary measurements on the classification results
    def fit_pred(self, trainSet, testSetName, count_list):
        # Append all the data in the training list into one single dataframe
        trainDF = pd.DataFrame([])
        for i in range(0, len(trainSet)):
            trainDF = trainDF.append(trainSet[i])
        trainDF = trainDF.reset_index(drop=True)

        # Extract training data/labels and testing data/labels
        trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
        trainDF_labels = trainDF['LABEL']

        # Fit the model
        self.clf.fit(trainDF_data, trainDF_labels)

        # Save the training model
        joblib.dump(self.clf, 'NRT_' + testSetName + '.pkl')


def main():
    basepath = '/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/instances_O12L0P24/'

    tsf = NRT_TSF()

    # Data reading with binary labels
    partition2_nf, test_nf_p2 = tsf.data_reader(basepath, 'nrt_p2', 'NF')
    partition2_fl, test_fl_p2 = tsf.data_reader(basepath, 'nrt_p2', 'FL')
    partition2 = partition2_nf.append(partition2_fl, ignore_index=True)

    partition3_nf, test_nf_p3 = tsf.data_reader(basepath, 'nrt_p3', 'NF')
    partition3_fl, test_fl_p3 = tsf.data_reader(basepath, 'nrt_p3', 'FL')
    partition3 = partition3_nf.append(partition3_fl, ignore_index=True)

    partition4_nf, train_nf_p4 = tsf.data_reader(basepath, 'nrt_p4', 'NF')
    partition4_fl, train_fl_p4 = tsf.data_reader(basepath, 'nrt_p4', 'FL')
    partition4 = partition4_nf.append(partition4_fl, ignore_index=True)

    partition5_nf, train_nf_p5 = tsf.data_reader(basepath, 'nrt_p5', 'NF')
    partition5_fl, train_fl_p5 = tsf.data_reader(basepath, 'nrt_p5', 'FL')
    partition5 = partition5_nf.append(partition5_fl, ignore_index=True)

    count_nf = test_nf_p2 + test_nf_p3 + train_nf_p4 + train_nf_p5
    count_fl = test_fl_p2 + test_fl_p3 + train_fl_p4 + train_fl_p5

    count = [count_fl, count_nf]
    trainSet = [partition2, partition3, partition4, partition5]
    tsf.fit_pred(trainSet, 'TRp2p3p4p5', count)
    #result.to_csv("/home/aji1/FP/test_class/DSDO_result.csv")


if __name__ == '__main__':
    main()

# pip install sktime


# pip install pandas


# pip install numpy




