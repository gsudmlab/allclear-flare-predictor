import glob
import joblib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import Measurements as measurements
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sktime.classification.compose import ColumnEnsembleClassifier
from sktime.classification.compose import TimeSeriesForestClassifier
from sktime.transformers.series_as_features.compose import ColumnConcatenator


# This method extract the necessary features from the destination folder
def data_reader_binary(loadPath, partitions, flare_label):
    # Read files from the define path
    all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
    count = len(all_files)

    li = []
    for filename in all_files:
        # Read the file and extract necessary features
        df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        # Define Label value based on the file name
        if flare_label == 'NF':
            LABEL = 'CBN'
        else:
            LABEL = 'XM'

        USFLUX = df['USFLUX']
        TOTUSJZ = df['TOTUSJZ']
        TOTUSJH = df['TOTUSJH']
        ABSNJZH = df['ABSNJZH']
        SAVNCPP = df['SAVNCPP']
        TOTPOT = df['TOTPOT']

        li.append([USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT, LABEL])
    # Create and return the dataframe build on the extracted features
    partition_frame = pd.DataFrame(li, columns=['USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT', 'LABEL'])
    return partition_frame, count


# This function converts multiple labels into binary labels
# by defining X, M as flaring class while C, B, N as non-flaring class
def convert_to_binary_class(label_col):
    for i in range(len(label_col)):
        if (label_col[i].startswith('X')) or (label_col[i].startswith('M')):
            label_col[i] = 'XM'
        else:
            label_col[i] = 'CBN'
    return label_col


# This function build a Time Series Forest Classifier and generate necessary measurements on the classification results
def TSF(trainSet, testSetName, count_list):
    # Append all the data in the training list into one single dataframe
    trainDF = pd.DataFrame([])
    for i in range(0, len(trainSet)):
        trainDF = trainDF.append(trainSet[i])
    trainDF = trainDF.reset_index(drop=True)

    # Extract training data/labels and testing data/labels
    trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
    trainDF_labels = trainDF['LABEL']

    # Have a glance of the training and testing dimensions
    print('# Model: TSF,,,')

    # Ensemble the result from all the columns and build a Time Series Forest Classifier to perform multivariate time seires classification
    clf = ColumnEnsembleClassifier(estimators=[
        ("TSF0",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [0]),
        ("TSF1",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [1]),
        ("TSF2",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [2]),
        ("TSF3",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [3]),
        ("TSF4",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [4]),
        ("TSF5",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [5]),
    ])

    clf.fit(trainDF_data, trainDF_labels)
    # Save the training model
    joblib.dump(clf, 'Deploy_TRp2p3p4p5' + testSetName + '.pkl')


def main():
    basepath = '/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/instances_O12L0P24/'

    # Data reading with binary labels
    partition4_NF_binary, test_nf_p2 = data_reader_binary(basepath, 'nrt_p2', 'NF')
    partition4_FL_binary, test_fl_p2 = data_reader_binary(basepath, 'nrt_p2', 'FL')
    partition2 = partition4_NF_binary.append(partition4_FL_binary, ignore_index=True)

    partition5_NF_binary, test_nf_p3 = data_reader_binary(basepath, 'nrt_p3', 'NF')
    partition5_FL_binary, test_fl_p3 = data_reader_binary(basepath, 'nrt_p3', 'FL')
    partition3 = partition5_NF_binary.append(partition5_FL_binary, ignore_index=True)

    partition1_NF_binary, train_nf_p4 = data_reader_binary(basepath, 'nrt_p4', 'NF')
    partition1_FL_binary, train_fl_p4 = data_reader_binary(basepath, 'nrt_p4', 'FL')
    partition4 = partition1_NF_binary.append(partition1_FL_binary, ignore_index=True)

    partition2_NF_binary, train_nf_p5 = data_reader_binary(basepath, 'nrt_p5', 'NF')
    partition2_FL_binary, train_fl_p5 = data_reader_binary(basepath, 'nrt_p5', 'FL')
    partition5 = partition2_NF_binary.append(partition2_FL_binary, ignore_index=True)

    count_nf = test_nf_p2 + test_nf_p3 + train_nf_p4 + train_nf_p5
    count_fl = test_fl_p2 + test_fl_p3 + train_fl_p4 + train_fl_p5

    count = [count_fl, count_nf]
    trainSet_1_binary = [partition2, partition3, partition4, partition5]
    TSF(trainSet_1_binary, '', count)


if __name__ == '__main__':
    main()

# pip install sktime


# pip install pandas


# pip install numpy




