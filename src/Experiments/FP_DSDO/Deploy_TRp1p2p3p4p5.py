import glob
import joblib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import Measurements as measurements
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sktime.classification.compose import ColumnEnsembleClassifier
from sktime.classification.compose import TimeSeriesForestClassifier
from sktime.transformers.series_as_features.compose import ColumnConcatenator


# This method extract the necessary features from the destination folder
def data_reader_binary(loadPath, partitions, flare_label):
    # Read files from the define path
    all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
    count = len(all_files)

    li = []
    for filename in all_files:
        # Read the file and extract necessary features
        df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        # Define Label value based on the file name
        if flare_label == 'NF':
            LABEL = 'CBN'
        else:
            LABEL = 'XM'

        USFLUX = df['USFLUX']
        TOTUSJZ = df['TOTUSJZ']
        TOTUSJH = df['TOTUSJH']
        ABSNJZH = df['ABSNJZH']
        SAVNCPP = df['SAVNCPP']
        TOTPOT = df['TOTPOT']

        li.append([USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT, LABEL])
    # Create and return the dataframe build on the extracted features
    partition_frame = pd.DataFrame(li, columns=['USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT', 'LABEL'])
    return partition_frame, count


# This function converts multiple labels into binary labels
# by defining X, M as flaring class while C, B, N as non-flaring class
def convert_to_binary_class(label_col):
    for i in range(len(label_col)):
        if (label_col[i].startswith('X')) or (label_col[i].startswith('M')):
            label_col[i] = 'XM'
        else:
            label_col[i] = 'CBN'
    return label_col


# This function build a Time Series Forest Classifier and generate necessary measurements on the classification results
def TSF(trainSet, testSetName, count_list):
    # Append all the data in the training list into one single dataframe
    trainDF = pd.DataFrame([])
    for i in range(0, len(trainSet)):
        trainDF = trainDF.append(trainSet[i])
    trainDF = trainDF.reset_index(drop=True)

    # Extract training data/labels and testing data/labels
    trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
    trainDF_labels = trainDF['LABEL']

    # Have a glance of the training and testing dimensions
    #print('# Model: TSF,,,')
    #print('Training Dimensions: ' + str(trainDF_data.shape))
    #print('Testing Dimensions: ' + str(testDF_data.shape))
    #print()

    # Ensemble the result from all the columns and build a Time Series Forest Classifier to perform multivariate time seires classification
    clf = ColumnEnsembleClassifier(estimators=[
        ("TSF0",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [0]),
        ("TSF1",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [1]),
        ("TSF2",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [2]),
        ("TSF3",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [3]),
        ("TSF4",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [4]),
        ("TSF5",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=10), [5]),
    ])

    clf.fit(trainDF_data, trainDF_labels)
    #print('# Model Parameters: **' + str(clf).replace(',', ';').replace('\n', '').replace(' ', '') + '**,,,')
    #print('# TrainingSet: **/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/partition1 & partition3** , number of positives(XM): '
    #      + str(count_list[1] + count_list[3]) + ', number of negatives(CBN): ' + str(count_list[0] + count_list[2]) + ',')
    #print('# TestingSet: **/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/partition4 & partition5** , number of positives(XM): '
    #      + str(count_list[5] + count_list[7]) + ', number of negatives(CBN): ' + str(count_list[4] + count_list[6]) + ',')
    #print('# ModelPath: **/home/aji1/FP/FP_DSDO**,,,')

    # Save the training model
    joblib.dump(clf, 'Deploy_' + testSetName + '.pkl')


def main():
    basepath = '/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/'

    # Data reading with binary labels
    partition1_NF_binary, train_nf_p1 = data_reader_binary(basepath, 'partition1', 'NF')
    partition1_FL_binary, train_fl_p1 = data_reader_binary(basepath, 'partition1', 'FL')
    partition1 = partition1_NF_binary.append(partition1_FL_binary, ignore_index=True)

    partition2_NF_binary, train_nf_p2 = data_reader_binary(basepath, 'partition2', 'NF')
    partition2_FL_binary, train_fl_p2 = data_reader_binary(basepath, 'partition2', 'FL')
    partition2 = partition2_NF_binary.append(partition2_FL_binary, ignore_index=True)

    partition3_NF_binary, train_nf_p3 = data_reader_binary(basepath, 'partition3', 'NF')
    partition3_FL_binary, train_fl_p3 = data_reader_binary(basepath, 'partition3', 'FL')
    partition3 = partition3_NF_binary.append(partition3_FL_binary, ignore_index=True)

    partition4_NF_binary, test_nf_p4 = data_reader_binary(basepath, 'partition4', 'NF')
    partition4_FL_binary, test_fl_p4 = data_reader_binary(basepath, 'partition4', 'FL')
    partition4 = partition4_NF_binary.append(partition4_FL_binary, ignore_index=True)

    partition5_NF_binary, test_nf_p5 = data_reader_binary(basepath, 'partition5', 'NF')
    partition5_FL_binary, test_fl_p5 = data_reader_binary(basepath, 'partition5', 'FL')
    partition5 = partition5_NF_binary.append(partition5_FL_binary, ignore_index=True)

    train_nf = train_nf_p1 + train_nf_p2 + train_nf_p3 + test_nf_p4 + test_nf_p5
    train_fl = train_fl_p1 + train_fl_p2 + train_fl_p3 + test_fl_p4 + test_fl_p5

    count = [train_nf, train_fl]
    trainSet_1_binary = [partition1, partition2, partition3, partition4, partition5]
    TSF(trainSet_1_binary, 'TRp1p2p3p4p5', count)


if __name__ == '__main__':
    main()

# pip install sktime


# pip install pandas


# pip install numpy




