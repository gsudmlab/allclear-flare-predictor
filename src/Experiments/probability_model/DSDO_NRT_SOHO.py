import glob
import joblib
import pandas as pd
import numpy as np
from sklearn.metrics import confusion_matrix
from sklearn.tree import DecisionTreeClassifier
import Measurements as Measurements


def get_models():
    """Generate a library of base learners."""
    # Append all the data in the training list into one single dataframe
    DSDO = np.load('/home/aji1/FP/FP_DSDO/Exp3_TRp2p3_TE_NSDOp4p5.npy')
    NRT = np.load('/home/aji1/FP/FP_NRT/TRp2p3_TEp4p5.npy')
    SOHO = np.load('/home/aji1/FP/FP_SOHO/Depl_TRp1p2p3_TE_NSDOp4p5.npy')

    models = {'DSDO_TRp2p3': DSDO,
              'NRT_TRp2p3': NRT,
              'SOHO_p1p2p3': SOHO
              }

    return models


def predict_weighted_proba(pred_results, W):
    if len(pred_results) != len(W):
        print('The size of the weights should match the size of the prediction labels')
    else:
        pred_proba = np.copy(pred_results)
        for i in range(len(W)):
            pred_proba[i] = pred_results[i] * W[i]

        result_sum = np.sum(pred_proba, axis=0) / np.sum(W)
        return result_sum


predict_weighted_proba([DSDO,NRT,SOHO],[0.61,0.65,0.34])
