import glob
import joblib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import Measurements as measurements
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sktime.classification.compose import ColumnEnsembleClassifier
from sktime.classification.compose import TimeSeriesForestClassifier
from sktime.transformers.series_as_features.compose import ColumnConcatenator


# This method extract the necessary features from the destination folder
def data_reader_binary(loadPath, partitions, flare_label):
    # Read files from the define path
    all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")

    # Select only FQ and not C-class files
    files = []
    if flare_label == 'ER':
        for file in all_files:
            conf = file.split('_Conf:')[-1]
            conf_n = conf.split(':Primary_ar')[0]
            conf_n = conf_n.split(':Secondary_ar')[0]
            if float(conf_n) > 2.0:
                files.append(file)
    else:
        files = all_files

    count = len(files)

    li = []
    for filename in files:
        # Read the file and extract necessary features
        df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        # Define Label value based on the file name
        if flare_label == 'FQ':
            LABEL = 0
        else:
            LABEL = 1

        USFLUX = df['USFLUX']
        TOTUSJZ = df['TOTUSJZ']
        TOTUSJH = df['TOTUSJH']
        ABSNJZH = df['ABSNJZH']
        SAVNCPP = df['SAVNCPP']
        TOTPOT = df['TOTPOT']

        li.append([USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT, LABEL])
    # Create and return the dataframe build on the extracted features
    partition_frame = pd.DataFrame(li, columns=['USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT', 'LABEL'])
    return partition_frame, count


# This function build a Time Series Forest Classifier and generate necessary measurements on the classification results
def TSF(trainSet, testSetName):
    # Append all the data in the training list into one single dataframe
    trainDF = pd.DataFrame([])
    for i in range(0, len(trainSet)):
        trainDF = trainDF.append(trainSet[i])
    trainDF = trainDF.reset_index(drop=True)

    # Extract training data/labels and testing data/labels
    trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
    trainDF_labels = trainDF['LABEL']

    # Have a glance of the training and testing dimensions
    #print('# Model: TSF,,,')

    # Ensemble the result from all the columns and build a Time Series Forest Classifier to perform multivariate time seires classification
    clf = ColumnEnsembleClassifier(estimators=[
        ("TSF0",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [0]),
        ("TSF1",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [1]),
        ("TSF2",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [2]),
        ("TSF3",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [3]),
        ("TSF4",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [4]),
        ("TSF5",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [5]),
    ])

    clf.fit(trainDF_data, trainDF_labels)
    #print('# Model Parameters: **' + str(clf).replace(',', ';').replace('\n', '').replace(' ', '') + '**,,,')
    #print('# TrainingSet: **/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/ER_instances_O12L0P24/nrt_p4 & nrt_p5** , number of FQ: '
    #    + str(count_list[0]) + ', number of ER: ' + str(count_list[1]) + ', number of NEFL: ' + str(count_list[2]))
    #print('# TestingSet: **/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/ER_instances_O12L0P24/nrt_p2 & nrt_p3** , number of FQ: '
    #    + str(count_list[3]) + ', number of ER: ' + str(count_list[4]) + ', number of NEFL: ' + str(count_list[5]))
    #print('# ModelPath: **/home/aji1/FP/FP_EP**,,,')

    # Save the training model
    joblib.dump(clf, 'Deploy_TRp2p3p4p5' + testSetName + '.pkl')


def main():
    basepath = '/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/ER_instances_O12L0P24/'

    # Data reading with binary labels
    partition2_FQ, train_fq_p2 = data_reader_binary(basepath, 'nrt_p2', 'FQ')
    partition2_ER, train_er_p2 = data_reader_binary(basepath, 'nrt_p2', 'ER')
    partition2_NEFL, train_nefl_p2 = data_reader_binary(basepath, 'nrt_p2', 'NEFL')
    partition2_FQ_ER = partition2_ER.append(partition2_FQ, ignore_index=True)
    partition2 = partition2_NEFL.append(partition2_FQ_ER, ignore_index=True)

    partition3_FQ, train_fq_p3 = data_reader_binary(basepath, 'nrt_p3', 'FQ')
    partition3_ER, train_er_p3 = data_reader_binary(basepath, 'nrt_p3', 'ER')
    partition3_NEFL, train_nefl_p3 = data_reader_binary(basepath, 'nrt_p3', 'NEFL')
    partition3_FQ_ER = partition3_ER.append(partition3_FQ, ignore_index=True)
    partition3 = partition3_NEFL.append(partition3_FQ_ER, ignore_index=True)

    partition4_FQ, test_fq_p4 = data_reader_binary(basepath, 'nrt_p4', 'FQ')
    partition4_ER, test_er_p4 = data_reader_binary(basepath, 'nrt_p4', 'ER')
    partition4_NEFL, test_nefl_p4 = data_reader_binary(basepath, 'nrt_p4', 'NEFL')
    partition4_FQ_ER = partition4_ER.append(partition4_FQ, ignore_index=True)
    partition4 = partition4_NEFL.append(partition4_FQ_ER, ignore_index=True)

    partition5_FQ, test_fq_p5 = data_reader_binary(basepath, 'nrt_p5', 'FQ')
    partition5_ER, test_er_p5 = data_reader_binary(basepath, 'nrt_p5', 'ER')
    partition5_NEFL, test_nefl_p5 = data_reader_binary(basepath, 'nrt_p5', 'NEFL')
    partition5_FQ_ER = partition5_ER.append(partition5_FQ, ignore_index=True)
    partition5 = partition5_NEFL.append(partition5_FQ_ER, ignore_index=True)

    #count = [train_count_FQ, train_count_ER, train_count_NEFL, test_count_FQ, test_count_ER, test_count_NEFL]
    trainSet = [partition2, partition3, partition4, partition5]
    TSF(trainSet, '')


if __name__ == '__main__':
    main()

# pip install sktime


# pip install pandas


# pip install numpy




