import glob
import joblib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import Measurements as measurements
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sktime.classification.compose import ColumnEnsembleClassifier
from sktime.classification.compose import TimeSeriesForestClassifier
from sktime.transformers.series_as_features.compose import ColumnConcatenator


# This method extract the necessary features from the destination folder
def data_reader_binary(loadPath, partitions, flare_label):
    # Read files from the define path
    all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")

    # Select only FQ and not C-class files
    files = []
    if flare_label == 'ER':
        for file in all_files:
            conf = file.split('_Conf:')[-1]
            conf_n = conf.split(':Primary_ar')[0]
            conf_n = conf_n.split(':Secondary_ar')[0]
            if float(conf_n) > 2.0:
                files.append(file)
    else:
        files = all_files

    count = len(files)

    li = []
    for filename in files:
        # Read the file and extract necessary features
        df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        # Define Label value based on the file name
        if flare_label == 'FQ':
            LABEL = 0
        else:
            LABEL = 1

        USFLUX = df['USFLUX']
        TOTUSJZ = df['TOTUSJZ']
        TOTUSJH = df['TOTUSJH']
        ABSNJZH = df['ABSNJZH']
        SAVNCPP = df['SAVNCPP']
        TOTPOT = df['TOTPOT']

        li.append([USFLUX, TOTUSJZ, TOTUSJH, ABSNJZH, SAVNCPP, TOTPOT, LABEL])
    # Create and return the dataframe build on the extracted features
    partition_frame = pd.DataFrame(li, columns=['USFLUX', 'TOTUSJZ', 'TOTUSJH', 'ABSNJZH', 'SAVNCPP', 'TOTPOT', 'LABEL'])
    return partition_frame, count


# This function build a Time Series Forest Classifier and generate necessary measurements on the classification results
def TSF(trainSet, testSet, testSetName, count_list):
    # Append all the data in the training list into one single dataframe
    trainDF = pd.DataFrame([])
    testDF = pd.DataFrame([])
    for i in range(0, len(trainSet)):
        trainDF = trainDF.append(trainSet[i])
    trainDF = trainDF.reset_index(drop=True)

    for i in range(0, len(testSet)):
        testDF = testDF.append(testSet[i])
    testDF = testDF.reset_index(drop=True)

    # Extract training data/labels and testing data/labels
    trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
    trainDF_labels = trainDF['LABEL']
    testDF_data = testDF.loc[:, testDF.columns != 'LABEL']
    testDF_labels = testDF['LABEL']

    # Have a glance of the training and testing dimensions
    print('# Model: TSF,,,')

    # Ensemble the result from all the columns and build a Time Series Forest Classifier to perform multivariate time seires classification
    clf = ColumnEnsembleClassifier(estimators=[
        ("TSF0",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [0]),
        ("TSF1",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [1]),
        ("TSF2",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [2]),
        ("TSF3",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [3]),
        ("TSF4",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [4]),
        ("TSF5",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight='balanced',
                                    n_jobs=3), [5]),
    ])

    clf.fit(trainDF_data, trainDF_labels)
    print('# Model Parameters: **' + str(clf).replace(',', ';').replace('\n', '').replace(' ', '') + '**,,,')
    print('# TrainingSet: **/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/ER_instances_O12L0P24/nrt_p4 & nrt_p5** , number of FQ: '
        + str(count_list[0]) + ', number of ER: ' + str(count_list[1]) + ', number of NEFL: ' + str(count_list[2]))
    print('# TestingSet: **/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/ER_instances_O12L0P24/nrt_p2 & nrt_p3** , number of FQ: '
        + str(count_list[3]) + ', number of ER: ' + str(count_list[4]) + ', number of NEFL: ' + str(count_list[5]))
    print('# ModelPath: **/home/aji1/FP/FP_EP**,,,')

    # Save the training model
    joblib.dump(clf, 'Exp2_TRp4p5_' + testSetName + '.pkl')
    # Predict label based on testing data
    pred_labels = clf.predict(testDF_data)
    pred_proba = clf.predict_proba(testDF_data)
    np.save('Exp2_TRp4p5_' + testSetName + '.npy', pred_proba)
    # Convert multiple labels into binary labels
    #testDF_labels = convert_to_binary_class(testDF_labels)
    #pred_labels = convert_to_binary_class(pred_labels)
    # Generate a confusion matrix based on given and predicted labels
    scores = confusion_matrix(testDF_labels, pred_labels, labels=[0, 1]).ravel()
    tn, fp, fn, tp = scores
    print('# Confusion Matrix,,,')
    print('#,,Predicted,')
    print('#,,ER(+NEFL),NF')
    print('#Actual,ER(+NEFL),{0},{1}'.format(tp, fn))
    print('#,NF,{0},{1}'.format(fp, tn))
    print('#,,,')
    print('Metric,Score,,')

    results_DF = pd.DataFrame(columns=['Accur', 'TSS', 'HSS', 'GSS', 'ER+NEFL_Pr', 'TPR', 'NFPr', 'TNR', 'FAR', 'POFD', 'f1ER+NEFL', 'f1NF'], index=[0])

    # Accuracy
    scoreTrain = clf.score(trainDF_data, trainDF_labels)
    scoreTest = clf.score(testDF_data, testDF_labels)
    #print("Train Accuracy: " + str(round(scoreTrain, 4)))
    #print("Test Accuracy: " + str(round(scoreTest, 4)))
    results_DF['Accur'] = scoreTest

    # TSS
    tss = measurements.TSS(scores)
    print('TSS,{0},,'.format(tss))
    results_DF['TSS'] = tss

    # HSS2 Definition 2
    hss2 = measurements.HSS2(scores)
    print('HSS2,{0},,'.format(hss2))
    results_DF['HSS'] = hss2

    # GSS
    gss = measurements.GSS(scores)
    print('GSS,{0},,'.format(gss))
    results_DF['GSS'] = gss

    # Precision Positive
    posPrecision = measurements.precisionPos(scores)
    print('Precision(ER+NEFL),{0},,'.format(posPrecision))
    results_DF['ER+NEFLPr'] = posPrecision

    # TPR
    tpr = measurements.TPR(scores)
    print('TPR/Recall(ER+NEFL),{0},,'.format(tpr))
    results_DF['TPR'] = tpr

    # Precision Negative
    negPrecision = measurements.precisionNeg(scores)
    print('Precision(NF),{0},,'.format(negPrecision))
    results_DF['NFPr'] = negPrecision

    # TNR
    tnr = measurements.TNR(scores)
    print('TNR/Recall(NF),{0},,'.format(tnr))
    results_DF['TNR'] = tnr

    # FAR
    far = measurements.FAR(scores)
    print('FAR/FPR,{0},,'.format(far))
    results_DF['FAR'] = far

    # POFD
    pofd = measurements.POFD(scores)
    print('POFD/FDR,{0},,'.format(pofd))
    results_DF['POFD'] = pofd

    # F1(XM)
    f1XM = measurements.F1Pos(scores)
    print('F1(ER+NEFL),{0},,'.format(f1XM))
    results_DF['f1ER+NEFL'] = f1XM

    # F1(CBN)
    f1CBN = measurements.F1Neg(scores)
    print('F1(NF),{0},,'.format(f1CBN))
    results_DF['f1NF'] = f1CBN

    # Return the result measurement dataframe
    return results_DF


def main():
    basepath = '/data/SHARPS/BERKAY/NRT_v0.1/NRT_partitioned/ER_instances_O12L0P24/'

    # Data reading with binary labels
    partition2_FQ, train_fq_p2 = data_reader_binary(basepath, 'nrt_p4', 'FQ')
    partition2_ER, train_er_p2 = data_reader_binary(basepath, 'nrt_p4', 'ER')
    partition2_NEFL, train_nefl_p2 = data_reader_binary(basepath, 'nrt_p4', 'NEFL')
    partition2_FQ_ER = partition2_ER.append(partition2_FQ, ignore_index=True)
    partition2 = partition2_NEFL.append(partition2_FQ_ER, ignore_index=True)

    partition3_FQ, train_fq_p3 = data_reader_binary(basepath, 'nrt_p5', 'FQ')
    partition3_ER, train_er_p3 = data_reader_binary(basepath, 'nrt_p5', 'ER')
    partition3_NEFL, train_nefl_p3 = data_reader_binary(basepath, 'nrt_p5', 'NEFL')
    partition3_FQ_ER = partition3_ER.append(partition3_FQ, ignore_index=True)
    partition3 = partition3_NEFL.append(partition3_FQ_ER, ignore_index=True)

    train_count_FQ = train_fq_p2 + train_fq_p3
    train_count_ER = train_er_p2 + train_er_p3
    train_count_NEFL = train_nefl_p2 + train_nefl_p3

    partition4_FQ, test_fq_p4 = data_reader_binary(basepath, 'nrt_p2', 'FQ')
    partition4_ER, test_er_p4 = data_reader_binary(basepath, 'nrt_p2', 'ER')
    partition4_NEFL, test_nefl_p4 = data_reader_binary(basepath, 'nrt_p2', 'NEFL')
    partition4_FQ_ER = partition4_ER.append(partition4_FQ, ignore_index=True)
    partition4 = partition4_NEFL.append(partition4_FQ_ER, ignore_index=True)

    partition5_FQ, test_fq_p5 = data_reader_binary(basepath, 'nrt_p3', 'FQ')
    partition5_ER, test_er_p5 = data_reader_binary(basepath, 'nrt_p3', 'ER')
    partition5_NEFL, test_nefl_p5 = data_reader_binary(basepath, 'nrt_p3', 'NEFL')
    partition5_FQ_ER = partition5_ER.append(partition5_FQ, ignore_index=True)
    partition5 = partition5_NEFL.append(partition5_FQ_ER, ignore_index=True)

    test_count_FQ = test_fq_p4 + test_fq_p5
    test_count_ER = test_er_p4 + test_er_p5
    test_count_NEFL = test_nefl_p4 + test_nefl_p5

    count = [train_count_FQ, train_count_ER, train_count_NEFL, test_count_FQ, test_count_ER, test_count_NEFL]
    trainSet = [partition2, partition3]
    testSet = [partition4, partition5]
    TSF(trainSet, testSet, 'TEp2p3', count)


if __name__ == '__main__':
    main()

# pip install sktime


# pip install pandas


# pip install numpy




