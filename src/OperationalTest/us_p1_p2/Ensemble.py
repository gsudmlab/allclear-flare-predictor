import glob
import joblib
import numpy as np
import pandas as pd
from numpy import hstack
import matplotlib.pyplot as plt
from src import Measurements as measurements
from sklearn.pipeline import Pipeline
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import GradientBoostingClassifier
from sktime.classification.distance_based import ElasticEnsemble
from sktime.classification.compose import TimeSeriesForestClassifier
from sktime.classification.shapelet_based import ShapeletTransformClassifier
from sktime.transformers.series_as_features.compose import ColumnConcatenator

# This method extract the necessary features from the destination folder
def data_reader_binary(loadPath, partitions, flare_label):
    # Read files from the define path
    all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
    count = len(all_files) + 1

    li = []
    for filename in all_files:
        tag = filename.split('@')[0]
        tag = tag.split('/')[-1]
        tag_name = tag.split('_ar')[0]

        # Read the file and extract necessary features
        df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        # Define Label value based on the file name
        if flare_label == 'NF':
            LABEL = 'CBN'
        else:
            LABEL = 'XM'

        USFLUX = df['USFLUX']
        R_VALUE = df['R_VALUE']

        li.append([USFLUX, R_VALUE, LABEL, tag_name, filename])
    # Create and return the dataframe build on the extracted features
    partition_frame = pd.DataFrame(li, columns=['USFLUX', 'R_VALUE', 'LABEL', 'TAG', 'FILE_N'])
    return partition_frame, count

def get_models():
    """Generate a library of base learners."""
    # Append all the data in the training list into one single dataframe
    tsf1 = joblib.load('/home/aji1/SHARPS/OperationalTest/us_p1_p2/BaseTSFModel5_binary_p1&p2.pkl')
    tsf2 = joblib.load('/home/aji1/SHARPS/OperationalTest/us_p1_p2/BaseTSFModel6_binary_p1&p2.pkl')
    tsf3 = joblib.load('/home/aji1/SHARPS/OperationalTest/us_p1_p2/BaseTSFModel7_binary_p1&p2.pkl')
    tsf4 = joblib.load('/home/aji1/SHARPS/OperationalTest/us_p1_p2/BaseTSFModel8_binary_p1&p2.pkl')

    models = {'TimeSeriesForestClassifier1': tsf1,
              'TimeSeriesForestClassifier2': tsf2,
              'TimeSeriesForestClassifier3': tsf3,
              'TimeSeriesForestClassifier4': tsf4
              }

    return models

def predict_base_learners(pred_base_learners, input, verbose=True):
    """
    Generate a prediction matrix.
    """
    #P = np.zeros((input.shape[0], len(pred_base_learners))
    meta_X = list()

    for i, (name, m) in enumerate(pred_base_learners.items()):
        p = m.predict_proba(input)
        meta_X.append(p)
    meta_X = hstack(meta_X)

    return meta_X

def ensemble_predict(base_learners, meta_learner, input, verbose=True):
    """
    Generate predictions from the ensemble.
    """
    P_pred = predict_base_learners(base_learners, input, verbose=verbose)
    return meta_learner.predict(P_pred)

def fn_detect(testDF_labels, pred_labels, file_name, tag):
    fn_list_USFLUX = []
    fn_list_R = []
    cols = []
    for count in range(60):
        cols.append('TimeStamp_' + str(count))
    cols.append('TAG')

    for i in range(len(testDF_labels)):
        if pred_labels[i] == 'CBN' and testDF_labels[i] == 'XM':
            df = pd.read_csv(file_name[i], index_col=None, header=0, sep='\t')
            df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)

            USFLUX = df['USFLUX'].tolist()
            R_VALUE = df['R_VALUE'].tolist()
            USFLUX.append(tag[i])
            R_VALUE.append(tag[i])

            fn_list_USFLUX.append(USFLUX)
            fn_list_R.append(R_VALUE)

    fn_frame_us = pd.DataFrame(fn_list_USFLUX, columns=cols)
    fn_frame_r = pd.DataFrame(fn_list_R, columns=cols)
    return fn_frame_us, fn_frame_r

def fp_detect(testDF_labels, pred_labels, file_name, tag):
    fp_list_USFLUX = []
    fp_list_R = []
    cols = []
    for count in range(60):
        cols.append('TimeStamp_' + str(count))
    cols.append('TAG')

    for i in range(len(testDF_labels)):
        if pred_labels[i] == 'XM' and testDF_labels[i] == 'CBN':
            df = pd.read_csv(file_name[i], index_col=None, header=0, sep='\t')
            df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)

            USFLUX = df['USFLUX'].tolist()
            R_VALUE = df['R_VALUE'].tolist()
            USFLUX.append(tag[i])
            R_VALUE.append(tag[i])

            fp_list_USFLUX.append(USFLUX)
            fp_list_R.append(R_VALUE)

    fp_frame_us = pd.DataFrame(fp_list_USFLUX, columns=cols)
    fp_frame_r = pd.DataFrame(fp_list_R, columns=cols)
    return fp_frame_us, fp_frame_r

# This function build a Shapelet Transform Classifier and generate necessary measurements on the classification results
def evaluation(testDF_data, testDF_labels, pred_labels):
    # Generate a confusion matrix based on given and predicted labels
    scores = confusion_matrix(testDF_labels, pred_labels, labels=["CBN", "XM"]).ravel()
    tn, fp, fn, tp = scores

    print('# Confusion Matrix,,,')
    print('#,,Predicted,')
    print('#,,XM,CBN')
    print('#Actual,XM,{0},{1}'.format(tp, fn))
    print('#,CBN,{0},{1}'.format(fp, tn))
    print('#,,,')
    print('Metric,Score,,')

    results_DF = pd.DataFrame(
        columns=['TSS', 'HSS', 'GSS', 'XMPr', 'TPR', 'CBNPr', 'TNR', 'FAR', 'POFD', 'f1XM', 'f1CBN'],
        index=[0])

    # TSS
    tss = measurements.TSS(scores)
    print('TSS,{0},,'.format(tss))
    results_DF['TSS'] = tss

    # HSS2 Definition 2
    hss2 = measurements.HSS2(scores)
    print('HSS2,{0},,'.format(hss2))
    results_DF['HSS'] = hss2

    # GSS
    gss = measurements.GSS(scores)
    print('GSS,{0},,'.format(gss))
    results_DF['GSS'] = gss

    # Precision Positive
    posPrecision = measurements.precisionPos(scores)
    print('Precision(XM),{0},,'.format(posPrecision))
    results_DF['XMPr'] = posPrecision

    # TPR
    tpr = measurements.TPR(scores)
    print('TPR/Recall(XM),{0},,'.format(tpr))
    results_DF['TPR'] = tpr

    # Precision Negative
    negPrecision = measurements.precisionNeg(scores)
    print('Precision(CBN),{0},,'.format(negPrecision))
    results_DF['CBNPr'] = negPrecision

    # TNR
    tnr = measurements.TNR(scores)
    print('TNR/Recall(CBN),{0},,'.format(tnr))
    results_DF['TNR'] = tnr

    # FAR
    far = measurements.FAR(scores)
    print('FAR/FPR,{0},,'.format(far))
    results_DF['FAR'] = far

    # POFD
    pofd = measurements.POFD(scores)
    print('POFD/FDR,{0},,'.format(pofd))
    results_DF['POFD'] = pofd

    # F1(XM)
    f1XM = measurements.F1Pos(scores)
    print('F1(XM),{0},,'.format(f1XM))
    results_DF['f1XM'] = f1XM

    # F1(CBN)
    f1CBN = measurements.F1Neg(scores)
    print('F1(CBN),{0},,'.format(f1CBN))
    results_DF['f1CBN'] = f1CBN

    # Return the result measurement dataframe
    return results_DF

def main():
    basepath = '/data/SHARPS/BERKAY/v0.7/CLUS/'
    testpath = '/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/'

    # Data reading with binary labels
    partition3_NF_binary, train_nf = data_reader_binary(basepath, 'partition3', 'NF')
    partition3_FL_binary, train_fl = data_reader_binary(basepath, 'partition3', 'FL')
    partition3_frame_binary = partition3_NF_binary.append(partition3_FL_binary, ignore_index=True)

    partition4_NF_binary, test_nf = data_reader_binary(testpath, 'partition4', 'NF')
    partition4_FL_binary, test_fl = data_reader_binary(testpath, 'partition4', 'FL')
    partition4_frame_binary = partition4_NF_binary.append(partition4_FL_binary, ignore_index=True)

    partition3_frame_binary.pop('FILE_N')
    partition3_frame_binary.pop('TAG')
    file_name = partition4_frame_binary.pop('FILE_N')
    tag = partition4_frame_binary.pop('TAG')

    trainDF_data = partition3_frame_binary.loc[:, partition3_frame_binary.columns != 'LABEL']
    trainDF_labels = partition3_frame_binary['LABEL']
    testDF_data = partition4_frame_binary.loc[:, partition4_frame_binary.columns != 'LABEL']
    testDF_labels = partition4_frame_binary['LABEL']

    print('# Model: DecisionTree,,,')
    base_learners = get_models()
    meta_learner = DecisionTreeClassifier(max_depth=5, class_weight='balanced')
    print('# Model Parameters: **' + str(meta_learner).replace(',', ';').replace('\n', '').replace(' ', '') + '**,,,')
    print('# TrainingSet: **/data/SHARPS/BERKAY/v0.7/CLUS/partition3** , number of positives(XM): '
          + str(train_fl) + ', number of negatives(CBN): ' + str(train_nf) + ',')
    print('# TestingSet: **/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/partition4** , number of positives(XM): '
          + str(test_fl) + ', number of negatives(CBN): ' + str(test_nf) + ',')
    print('# ModelPath: **/home/aji1/SHARPS/OperationalTest/us_p1_p2**,,,')

    P_base = predict_base_learners(base_learners, trainDF_data)

    meta_learner.fit(P_base, trainDF_labels)

    # Save the training model
    joblib.dump(meta_learner, 'EnsembleModel.pkl')

    p = ensemble_predict(base_learners, meta_learner, testDF_data)

    evaluation(testDF_data, testDF_labels, p)

    fn_frame_us, fn_frame_r = fn_detect(testDF_labels, p, file_name, tag)
    fn_frame_us.to_csv('/home/aji1/SHARPS/OperationalTest/us_p1_p2/fn_detection_usflux.csv')
    fn_frame_r.to_csv('/home/aji1/SHARPS/OperationalTest/us_p1_p2/fn_detection_r.csv')

    fp_frame_us, fp_frame_r = fp_detect(testDF_labels, p, file_name, tag)
    fp_frame_us.to_csv('/home/aji1/SHARPS/OperationalTest/us_p1_p2/fp_detection_usflux.csv')
    fp_frame_r.to_csv('/home/aji1/SHARPS/OperationalTest/us_p1_p2/fp_detection_r.csv')


if __name__ == '__main__':
    main()

#pip install sktime


#pip install pandas


#pip install numpy



