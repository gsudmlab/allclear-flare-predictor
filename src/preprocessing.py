import glob
import joblib
import pandas as pd
from numpy import hstack
from sklearn.metrics import confusion_matrix
from sklearn.tree import DecisionTreeClassifier

class prediction_input():
    def __init__(self, loadPath: str, *argv, **kwargs):
        self.loadPath = loadPath
        pass

    # This method extract the necessary features from the destination folder
    def preprocessing(self, partitions, flare_label):
        # Read files from the define path
        all_files = glob.glob(str(self.loadPath) + partitions + "/" + flare_label + "/*.csv")
        count = len(all_files) + 1

        li = []
        for filename in all_files:
            # Read the file and extract necessary features
            df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
            df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
            # Define Label value based on the file name
            if flare_label == 'NF':
                LABEL = 'CBN'
            else:
                LABEL = 'XM'

            USFLUX = df['USFLUX']
            R_VALUE = df['R_VALUE']

            li.append([USFLUX, R_VALUE, LABEL])
        # Create and return the dataframe build on the extracted features
        partition_frame = pd.DataFrame(li, columns=['USFLUX', 'R_VALUE', 'LABEL'])
        return partition_frame, count
