import glob
import joblib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import Measurements as measurements
from sklearn.pipeline import Pipeline
from sklearn.metrics import confusion_matrix
from sktime.classification.compose import ColumnEnsembleClassifier
from sktime.classification.compose import TimeSeriesForestClassifier
from sktime.transformers.series_as_features.compose import ColumnConcatenator


# This method extract the necessary features from the destination folder
def data_reader_binary(loadPath, partitions, flare_label):
    # Read files from the define path
    all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
    count = len(all_files) + 1

    li = []
    for filename in all_files:
        # Read the file and extract necessary features
        df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
        #print("File " + filename + " successfully readed")
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        # Define Label value based on the file name
        if flare_label == 'NF':
            LABEL = 'CBN'
        else:
            LABEL = 'XM'

        USFLUX = df['USFLUX']
        #MEANGBZ = df['MEANGBZ']
        R_VALUE = df['R_VALUE']
        #FDIM = df['FDIM']
        #PIL_LEN = df['PIL_LEN']

        li.append([USFLUX, R_VALUE, LABEL])

    # Create and return the dataframe build on the extracted features
    partition_frame = pd.DataFrame(li, columns=['USFLUX', 'R_VALUE', 'LABEL'])
    return partition_frame, count


# This function converts multiple labels into binary labels
# by defining X, M as flaring class while C, B, N as non-flaring class
def convert_to_binary_class(label_col):
    for i in range(len(label_col)):
        if (label_col[i].startswith('X')) or (label_col[i].startswith('M')):
            label_col[i] = 'XM'
        else:
            label_col[i] = 'CBN'
    return label_col


# This function build a Time Series Forest Classifier and generate necessary measurements on the classification results
def TSF(trainSet, testSet, testSetName, count_list):
    # Append all the data in the training list into one single dataframe
    trainDF = pd.DataFrame([])
    testDF = testSet
    for i in range(0, len(trainSet)):
        trainDF = trainDF.append(trainSet[i])
    trainDF = trainDF.reset_index(drop=True)

    # Extract training data/labels and testing data/labels
    trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
    trainDF_labels = trainDF['LABEL']
    testDF_data = testDF.loc[:, testDF.columns != 'LABEL']
    testDF_labels = testDF['LABEL']

    # Have a glance of the training and testing dimensions
    print('# Model: TSF,,,')
    #print('Training Dimensions: ' + str(trainDF_data.shape))
    #print('Testing Dimensions: ' + str(testDF_data.shape))
    #print()

    # Concatenate all the columns and build a Time Series Forest Classifier to perform multivariate time seires classification
    clf = ColumnEnsembleClassifier(estimators=[
        ("TSF0",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight={0: 0.33, 1: 0.67},
                                    n_jobs=10), [0]),
        ("TSF1",
         TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight={0: 0.33, 1: 0.67},
                                    n_jobs=10), [1]),
    ])

    clf.fit(trainDF_data, trainDF_labels)
    print('# Model Parameters: **' + str(clf).replace(',', ';').replace('\n', '').replace(' ', '') + '**,,,')
    print('# TrainingSet: **/data/ARIAP_partitioned/SOHOpartitions/instances_O12L0P24C6/SOHOp1 &SOHOp3** , number of positives(XM): '
          + str(count_list[1] + count_list[3]) + ', number of negatives(CBN): ' + str(count_list[0] + count_list[2]) + ',,,')
    print('# TestingSet: **/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/partition2** , number of positives(XM): '
          + str(count_list[5]) + ', number of negatives(CBN): ' + str(count_list[4]) + ',,,')
    print('# ModelPath: **/home/aji1/ARIAP/SOHO_vs_SDO**,,,')

    # Save the training model
    joblib.dump(clf, 'SOHO_base_' + testSetName + '.pkl')
    # Predict label based on testing data
    pred_labels = clf.predict(testDF_data)
    # Convert multiple labels into binary labels
    testDF_labels = convert_to_binary_class(testDF_labels)
    pred_labels = convert_to_binary_class(pred_labels)
    # Generate a confusion matrix based on given and predicted labels
    scores = confusion_matrix(testDF_labels, pred_labels, labels=["CBN", "XM"]).ravel()
    tn, fp, fn, tp = scores
    print('# Confusion Matrix,,,')
    print('#,,Predicted,')
    print('#,,XM,CBN')
    print('#Actual,XM,{0},{1}'.format(tp, fn))
    print('#,CBN,{0},{1}'.format(fp, tn))
    print('#,,,')
    print('Metric,Score,,')

    results_DF = pd.DataFrame(columns=['Accur', 'TSS', 'HSS', 'GSS', 'XMPr', 'TPR', 'CBNPr', 'TNR', 'FAR', 'POFD', 'f1XM', 'f1CBN'], index=[0])

    # Accuracy
    #scoreTrain = clf.score(trainDF_data, trainDF_labels)
    scoreTest = clf.score(testDF_data, testDF_labels)
    #print("Train Accuracy: " + str(round(scoreTrain, 4)))
    #print("Test Accuracy: " + str(round(scoreTest, 4)))
    results_DF['Accur'] = scoreTest

    # TSS
    tss = measurements.TSS(scores)
    print('TSS,{0},,'.format(tss))
    results_DF['TSS'] = tss

    # HSS2 Definition 2
    hss2 = measurements.HSS2(scores)
    print('HSS2,{0},,'.format(hss2))
    results_DF['HSS'] = hss2

    # GSS
    gss = measurements.GSS(scores)
    print('GSS,{0},,'.format(gss))
    results_DF['GSS'] = gss

    # Precision Positive
    posPrecision = measurements.precisionPos(scores)
    print('Precision(XM),{0},,'.format(posPrecision))
    results_DF['XMPr'] = posPrecision

    # TPR
    tpr = measurements.TPR(scores)
    print('TPR/Recall(XM),{0},,'.format(tpr))
    results_DF['TPR'] = tpr

    # Precision Negative
    negPrecision = measurements.precisionNeg(scores)
    print('Precision(CBN),{0},,'.format(negPrecision))
    results_DF['CBNPr'] = negPrecision

    # TNR
    tnr = measurements.TNR(scores)
    print('TNR/Recall(CBN),{0},,'.format(tnr))
    results_DF['TNR'] = tnr

    # FAR
    far = measurements.FAR(scores)
    print('FAR/FPR,{0},,'.format(far))
    results_DF['FAR'] = far

    # POFD
    pofd = measurements.POFD(scores)
    print('POFD/FDR,{0},,'.format(pofd))
    results_DF['POFD'] = pofd

    # F1(XM)
    f1XM = measurements.F1Pos(scores)
    print('F1(XM),{0},,'.format(f1XM))
    results_DF['f1XM'] = f1XM

    # F1(CBN)
    f1CBN = measurements.F1Neg(scores)
    print('F1(CBN),{0},,'.format(f1CBN))
    results_DF['f1CBN'] = f1CBN

    # Return the result measurement dataframe
    return results_DF


def main():
    basepath = '/data/ARIAP_partitioned/SOHOpartitions/instances_O12L0P24C6/'
    testpath = '/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/'

    # Data reading with binary labels
    partition1_NF_binary, train_nf_p1 = data_reader_binary(basepath, 'SOHOp1', 'NF')
    partition1_FL_binary, train_fl_p1 = data_reader_binary(basepath, 'SOHOp1', 'FL')
    partition1_frame_binary = partition1_NF_binary.append(partition1_FL_binary, ignore_index=True)

    partition2_NF_binary, train_nf_p2 = data_reader_binary(basepath, 'SOHOp3', 'NF')
    partition2_FL_binary, train_fl_p2 = data_reader_binary(basepath, 'SOHOp3', 'FL')
    partition2_frame_binary = partition2_NF_binary.append(partition2_FL_binary, ignore_index=True)

    partition4_NF_binary, test_nf = data_reader_binary(testpath, 'partition2', 'NF')
    partition4_FL_binary, test_fl = data_reader_binary(testpath, 'partition2', 'FL')
    partition4_frame_binary = partition4_NF_binary.append(partition4_FL_binary, ignore_index=True)

    count = [train_nf_p1, train_fl_p1, train_nf_p2, train_fl_p2, test_nf, test_fl]
    trainSet_1_binary = [partition1_frame_binary, partition2_frame_binary]
    testSet_1_binary = partition4_frame_binary
    TSF(trainSet_1_binary, testSet_1_binary, 'TestSDOp2', count)


if __name__ == '__main__':
    main()

# pip install sktime


# pip install pandas


# pip install numpy




