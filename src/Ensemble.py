import glob
import joblib
import pandas as pd
from numpy import hstack
import CONSTANTS as cons
from src import Measurements
from dtreeviz.trees import dtreeviz
from sklearn.metrics import confusion_matrix
from sklearn.tree import DecisionTreeClassifier

# This method extract the necessary features from the destination folder
def data_reader_binary(loadPath, partitions, flare_label):
    # Read files from the define path
    all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
    count = len(all_files) + 1

    li = []
    for filename in all_files:
        # Read the file and extract necessary features
        df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        # Define Label value based on the file name
        if flare_label == 'NF':
            LABEL = 'CBN'
        else:
            LABEL = 'XM'

        USFLUX = df['USFLUX']
        R_VALUE = df['R_VALUE']

        li.append([USFLUX, R_VALUE, LABEL])
    # Create and return the dataframe build on the extracted features
    partition_frame = pd.DataFrame(li, columns=['USFLUX', 'R_VALUE', 'LABEL'])
    return partition_frame, count

def get_models():
    """Generate a library of base learners."""
    # Append all the data in the training list into one single dataframe
    tsf1 = joblib.load('/home/aji1/SHARPS/Ensemble/TSF_updated/BaseTSFModel5_binary_partition1.pkl')
    tsf2 = joblib.load('/home/aji1/SHARPS/Ensemble/TSF_updated/BaseTSFModel6_binary_partition1.pkl')
    tsf3 = joblib.load('/home/aji1/SHARPS/Ensemble/TSF_updated/BaseTSFModel7_binary_partition1.pkl')
    tsf4 = joblib.load('/home/aji1/SHARPS/Ensemble/TSF_updated/BaseTSFModel8_binary_partition1.pkl')

    models = {'TimeSeriesForestClassifier1': tsf1,
              'TimeSeriesForestClassifier2': tsf2,
              'TimeSeriesForestClassifier3': tsf3,
              'TimeSeriesForestClassifier4': tsf4
              }

    return models

def predict_base_learners(pred_base_learners, input, verbose=True):
    """
    Generate a prediction matrix.
    """
    #P = np.zeros((input.shape[0], len(pred_base_learners))
    meta_X = list()

    for i, (name, m) in enumerate(pred_base_learners.items()):
        p = m.predict_proba(input)
        meta_X.append(p)
    meta_X = hstack(meta_X)

    return meta_X

def ensemble_predict(base_learners, meta_learner, input, verbose=True):
    """
    Generate predictions from the ensemble.
    """
    P_pred = predict_base_learners(base_learners, input, verbose=verbose)
    return meta_learner.predict(P_pred)

# This function build a Shapelet Transform Classifier and generate necessary measurements on the classification results
def evaluation(trainDF_data, trainDF_labels, testDF_data, testDF_labels, pred_labels, clf):

    # Generate a confusion matrix based on given and predicted labels
    scores = confusion_matrix(testDF_labels, pred_labels, labels=["CBN", "XM"]).ravel()
    tn, fp, fn, tp = scores

    print('# Confusion Matrix,,,')
    print('#,,Predicted,')
    print('#,,XM,CBN')
    print('#Actual,XM,{0},{1}'.format(tp, fn))
    print('#,CBN,{0},{1}'.format(fp, tn))
    print('#,,,')
    print('Metric,Score,,')

    results_DF = pd.DataFrame(
        columns=['TSS', 'HSS', 'GSS', 'XMPr', 'TPR', 'CBNPr', 'TNR', 'FAR', 'POFD', 'f1XM', 'f1CBN'],
        index=[0])

    # TSS
    tss = Measurements.TSS(scores)
    print('TSS,{0},,'.format(tss))
    results_DF['TSS'] = tss

    # HSS2 Definition 2
    hss2 = Measurements.HSS2(scores)
    print('HSS2,{0},,'.format(hss2))
    results_DF['HSS'] = hss2

    # GSS
    gss = Measurements.GSS(scores)
    print('GSS,{0},,'.format(gss))
    results_DF['GSS'] = gss

    # Precision Positive
    posPrecision = Measurements.precisionPos(scores)
    print('Precision(XM),{0},,'.format(posPrecision))
    results_DF['XMPr'] = posPrecision

    # TPR
    tpr = Measurements.TPR(scores)
    print('TPR/Recall(XM),{0},,'.format(tpr))
    results_DF['TPR'] = tpr

    # Precision Negative
    negPrecision = Measurements.precisionNeg(scores)
    print('Precision(CBN),{0},,'.format(negPrecision))
    results_DF['CBNPr'] = negPrecision

    # TNR
    tnr = Measurements.TNR(scores)
    print('TNR/Recall(CBN),{0},,'.format(tnr))
    results_DF['TNR'] = tnr

    # FAR
    far = Measurements.FAR(scores)
    print('FAR/FPR,{0},,'.format(far))
    results_DF['FAR'] = far

    # POFD
    pofd = Measurements.POFD(scores)
    print('POFD/FDR,{0},,'.format(pofd))
    results_DF['POFD'] = pofd

    # F1(XM)
    f1XM = Measurements.F1Pos(scores)
    print('F1(XM),{0},,'.format(f1XM))
    results_DF['f1XM'] = f1XM

    # F1(CBN)
    f1CBN = Measurements.F1Neg(scores)
    print('F1(CBN),{0},,'.format(f1CBN))
    results_DF['f1CBN'] = f1CBN

    # Return the result measurement dataframe
    return results_DF

def main():
    basepath = cons.DATA_LOAD_PATH

    # Data reading with binary labels
    partition3_NF_binary, train_nf = data_reader_binary(basepath,'partition3','NF')
    partition3_FL_binary, train_fl = data_reader_binary(basepath,'partition3','FL')
    partition3_frame_binary = partition3_NF_binary.append(partition3_FL_binary, ignore_index=True)

    partition4_NF_binary, test_nf = data_reader_binary(basepath,'partition4','NF')
    partition4_FL_binary, test_fl = data_reader_binary(basepath,'partition4','FL')
    partition4_frame_binary = partition4_NF_binary.append(partition4_FL_binary, ignore_index=True)

    trainDF_data = partition3_frame_binary.loc[:, partition3_frame_binary.columns != 'LABEL']
    trainDF_labels = partition3_frame_binary['LABEL']
    testDF_data = partition4_frame_binary.loc[:, partition4_frame_binary.columns != 'LABEL']
    testDF_labels = partition4_frame_binary['LABEL']

    print('# Model: DecisionTree,,,')
    base_learners = get_models()
    meta_learner = DecisionTreeClassifier(max_depth=5, class_weight='balanced')
    print('# Model Parameters: **' + str(meta_learner).replace(',', ';').replace('\n', '').replace(' ', '') + '**,,,')
    print('# TrainingSet: **/data/SHARPS/BERKAY/v0.7/CLUS/partition3** , number of positives(XM): '
          + str(train_fl) + ', number of negatives(CBN): ' + str(train_nf) + ',')
    print('# TestingSet: **/data/SHARPS/BERKAY/v0.7/CLUS/partition4** , number of positives(XM): '
          + str(test_fl) + ', number of negatives(CBN): ' + str(test_nf) + ',')
    print('# ModelPath: **/home/aji1/SHARPS/Ensemble/TSF_updated**,,,')


    P_base = predict_base_learners(base_learners, trainDF_data)

    meta_learner.fit(P_base, trainDF_labels)
    joblib.dump(meta_learner, 'MetaModel.pkl')
    p = ensemble_predict(base_learners, meta_learner, testDF_data)

    evaluation(trainDF_data, trainDF_labels, testDF_data, testDF_labels, p, meta_learner)

    #viz = dtreeviz(meta_learner, X, y,
    #               target_name="target",
    #               feature_names=iris.feature_names,
    #               class_names=list(iris.target_names))

    #viz.savefig()


if __name__ == '__main__':
    main()

#pip install sktime


#pip install pandas


#pip install numpy



