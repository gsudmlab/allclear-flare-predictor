import glob
import joblib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import Measurements as measurements
from sklearn.pipeline import Pipeline
from sklearn.metrics import make_scorer
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_validate
from sklearn.model_selection import GroupKFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedShuffleSplit
from sktime.classification.compose import ColumnEnsembleClassifier
from sktime.classification.compose import TimeSeriesForestClassifier
from sktime.transformers.series_as_features.compose import ColumnConcatenator


# This method extract the necessary features from the destination folder
def data_reader_binary(loadPath, partitions, flare_label):
    # Read files from the define path
    all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
    count = len(all_files) + 1

    li = []
    for filename in all_files:
        # Read the file and extract necessary features
        df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
        df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)
        # Define Label value based on the file name
        if flare_label == 'NF':
            LABEL = 'CBN'
        else:
            LABEL = 'XM'

        USFLUX = df['USFLUX']
        R_VALUE = df['R_VALUE']

        li.append([USFLUX, R_VALUE, LABEL])
    # Create and return the dataframe build on the extracted features
    partition_frame = pd.DataFrame(li, columns=['USFLUX', 'R_VALUE', 'LABEL'])
    return partition_frame, count


# This function converts multiple labels into binary labels
# by defining X, M as flaring class while C, B, N as non-flaring class
def convert_to_binary_class(label_col):
    for i in range(len(label_col)):
        if (label_col[i].startswith('X')) or (label_col[i].startswith('M')):
            label_col[i] = 'XM'
        else:
            label_col[i] = 'CBN'
    return label_col

def hp_tuner(trainSet, testSet):
    trainDF = pd.DataFrame([])
    testDF = testSet
    train_p1_len = len(trainSet[0])
    train_p2_len = len(trainSet[1])
    train_p3_len = len(trainSet[2])
    for i in range(0, len(trainSet)):
        trainDF = trainDF.append(trainSet[i])
    trainDF = trainDF.reset_index(drop=True)

    trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
    trainDF_labels = trainDF['LABEL']
    testDF_data = testDF.loc[:, testDF.columns != 'LABEL']
    testDF_labels = testDF['LABEL']
    
    hss_scoring = make_scorer(measurements.make_HSS2, greater_is_better=True)

    clf = ColumnEnsembleClassifier(estimators=[
        ("TSF0",
         TimeSeriesForestClassifier(n_jobs=20), [0]),
        ("TSF1",
         TimeSeriesForestClassifier(n_jobs=20), [1]),
    ])

    '''
    def custom_cv_2folds(X):
        n = X.shape[0]
        i = 1
        while i <= 2:
            idx = np.arange(n * (i - 1) / 2, n * i / 2, dtype=int)
            yield idx, idx
            i += 1
    cv = custom_cv_2folds(trainDF_data)
    '''

    groups = np.array([0] * train_p1_len + [1] * train_p2_len + [2] * train_p3_len)
    cv = GroupKFold(n_splits=3)
    train_index_1, train_index_2, train_index_3 = cv.split(trainDF_data, trainDF_labels, groups)
    custom_cv = [train_index_1, train_index_2, train_index_3]

    #cv = StratifiedShuffleSplit(n_splits=5, test_size=0.3, random_state=42)

    '''
    'TSF0__n_estimators': [100, 500, 1000], 'TSF0__max_depth': [3, 6, 9],
    'TSF0__class_weight': [{0: 1, 1: 0.1}, {0: 1, 1: 0.2}, {0: 1, 1: 0.3}, {0: 1, 1: 0.4},
     {0: 1, 1: 0.5}, {0: 1, 1: 0.6}, {0: 1, 1: 0.7}, {0: 1, 1: 0.8},
     {0: 1, 1: 0.9}, {0: 1, 1: 1}, {0: 1, 1: 3}, {0: 1, 1: 5},
     {0: 1, 1: 7}, {0: 1, 1: 9}, {0: 1, 1: 11}, {0: 1, 1: 13},
     {0: 1, 1: 15}, {0: 1, 1: 17}, {0: 1, 1: 19}, {0: 1, 1: 21},
     {0: 1, 1: 23}, {0: 1, 1: 25}, {0: 1, 1: 27}, {0: 1, 1: 29},
     {0: 1, 1: 31}, {0: 1, 1: 33}, {0: 1, 1: 35}, {0: 1, 1: 37},
     {0: 1, 1: 39}, {0: 1, 1: 41}, {0: 1, 1: 43}, {0: 1, 1: 45},
     {0: 1, 1: 47}, {0: 1, 1: 49}]
    '''

    #estimator = [100, 500, 1000]
    max_depth = [6]
    class_weight = [{0: 1, 1: 0.1}, {0: 1, 1: 0.2}, {0: 1, 1: 0.3}, {0: 1, 1: 0.4},
     {0: 1, 1: 0.5}, {0: 1, 1: 0.6}, {0: 1, 1: 0.7}, {0: 1, 1: 0.8},
     {0: 1, 1: 0.9}, {0: 1, 1: 1}, {0: 1, 1: 3}, {0: 1, 1: 5},
     {0: 1, 1: 7}, {0: 1, 1: 9}, {0: 1, 1: 11}, {0: 1, 1: 13},
     {0: 1, 1: 15}, {0: 1, 1: 17}, {0: 1, 1: 19}, {0: 1, 1: 21},
     {0: 1, 1: 23}, {0: 1, 1: 25}, {0: 1, 1: 27}, {0: 1, 1: 29},
     {0: 1, 1: 31}, {0: 1, 1: 33}, {0: 1, 1: 35}, {0: 1, 1: 37},
     {0: 1, 1: 39}, {0: 1, 1: 41}, {0: 1, 1: 43}, {0: 1, 1: 45},
     {0: 1, 1: 47}, {0: 1, 1: 49}]

    allFoldResults_DF = pd.DataFrame([])
    for i in range(3):
        for j in range(len(class_weight)):
            tuned_parameters = {'TSF0__n_estimators': [500], 'TSF0__max_depth': [max_depth[i]],
                                'TSF0__class_weight': [class_weight[j]],
                                'TSF1__n_estimators': [500], 'TSF1__max_depth': [max_depth[i]],
                                'TSF1__class_weight': [class_weight[j]]
                                }

            grid = GridSearchCV(
                clf, param_grid=tuned_parameters, cv=custom_cv,
                scoring=hss_scoring, refit=True, return_train_score=True, n_jobs=20
            )
            grid.fit(trainDF_data, trainDF_labels)

            scores = grid.score(testDF_data, testDF_labels)
            print("For %s, the best parameters are %s with with an accuracy score of %0.5f and a HSS2 score of %0.5f" % (
                hss_scoring, grid.best_params_, grid.best_score_, scores))

            print("Grid scores on development set:")
            print()
            means = grid.cv_results_['mean_test_score']
            stds = grid.cv_results_['std_test_score']
            for mean, std, params in zip(means, stds, grid.cv_results_['params']):
                print("%0.3f (+/-%0.03f) for %r"
                        % (mean, std * 2, params))
            print()

            # Return GridSearch results
            results = grid.cv_results_
            df_results = pd.DataFrame.from_dict(results)
            df_results['Predict_Score'] = scores
            allFoldResults_DF = allFoldResults_DF.append(df_results)

    return allFoldResults_DF

def main():
    basepath = '/data/SHARPS/BERKAY/v0.7/CLUS/'
    testpath = '/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/'

    # Data reading with binary labels
    partition1_NF_binary, train_nf_p1 = data_reader_binary(basepath, 'partition1', 'NF')
    partition1_FL_binary, train_fl_p1 = data_reader_binary(basepath, 'partition1', 'FL')
    partition1_frame_binary = partition1_NF_binary.append(partition1_FL_binary, ignore_index=True)

    partition2_NF_binary, train_nf_p2 = data_reader_binary(basepath, 'partition2', 'NF')
    partition2_FL_binary, train_fl_p2 = data_reader_binary(basepath, 'partition2', 'FL')
    partition2_frame_binary = partition2_NF_binary.append(partition2_FL_binary, ignore_index=True)

    partition3_NF_binary, train_nf_p3 = data_reader_binary(basepath, 'partition3', 'NF')
    partition3_FL_binary, train_fl_p3 = data_reader_binary(basepath, 'partition3', 'FL')
    partition3_frame_binary = partition3_NF_binary.append(partition3_FL_binary, ignore_index=True)

    partition4_NF_binary, test_nf = data_reader_binary(testpath, 'partition4', 'NF')
    partition4_FL_binary, test_fl = data_reader_binary(testpath, 'partition4', 'FL')
    partition4_frame_binary = partition4_NF_binary.append(partition4_FL_binary, ignore_index=True)

    trainSet_binary = [partition1_frame_binary, partition2_frame_binary, partition3_frame_binary]
    testSet_binary = partition4_frame_binary

    result = hp_tuner(trainSet_binary, testSet_binary)
    result.to_csv('/home/aji1/SHARPS/hp_tune/hp_results_est500_max6.csv', index=False, sep=',')

if __name__ == '__main__':
    main()




