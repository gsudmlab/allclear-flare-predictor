import glob
import joblib
import pandas as pd
#import CONSTANTS as constants
from sklearn.metrics import confusion_matrix
from sktime.classification.compose import ColumnEnsembleClassifier
from sktime.classification.compose import TimeSeriesForestClassifier


class TSF:
    def __init__(self, isBinary=True):
        self.isBinary = isBinary
        # Time Series Forest Classifier for performance of multivariate time seires classification
        self.clf = ColumnEnsembleClassifier(estimators=[
            ("TSF0",
             TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight={0: 0.33, 1: 0.67},
                                        n_jobs=10), [0]),
            ("TSF1",
             TimeSeriesForestClassifier(n_estimators=50, criterion='gini', max_depth=3, class_weight={0: 0.33, 1: 0.67},
                                        n_jobs=10), [1]),
        ])
        pass

    # Interpolate all the missing values
    def preprocessor(self, df):
        return df.interpolate(method='linear', axis=0, limit_direction='both', inplace=True)

    # Append all the data in the training list into one single dataframe
    def frame(self, trainPath, testPath, train_partition, test_partition):
        trainDF = pd.DataFrame([])
        train_nf_count = 0
        train_fl_count = 0
        test_nf_count = 0
        test_fl_count = 0
        for train in train_partition:
            train_nf, nf_count = self.data_reader(trainPath, train, 'NF')
            train_fl, fl_count = self.data_reader(trainPath, train, 'FL')
            train_nf_count = train_nf_count + nf_count
            train_fl_count = train_fl_count + fl_count
            train_frame = train_nf.append(train_fl, ignore_index=True)
            trainDF.append(train_frame)
        trainDF = trainDF.reset_index(drop=True)

        testDF = pd.DataFrame([])
        for test in test_partition:
            test_nf, nf_count = self.data_reader(testPath, test, 'NF')
            test_fl, fl_count = self.data_reader(testPath, test, 'FL')
            test_nf_count = test_nf_count + nf_count
            test_fl_count = test_fl_count + fl_count
            test_frame = test_nf.append(test_fl, ignore_index=True)
            testDF.append(test_frame)
        testDF = testDF.reset_index(drop=True)

        count = [train_nf_count, train_fl_count, test_nf_count, test_fl_count]

        return trainDF, testDF, count

    # This method extract the necessary features from the destination folder
    def data_reader(self, loadPath, partitions, flare_label):
        # Read files from the define path
        all_files = glob.glob(str(loadPath) + partitions + "/" + flare_label + "/*.csv")
        count = len(all_files) + 1

        li = []
        for filename in all_files:
            if self.isBinary:
                if flare_label == 'NF':
                    LABEL = 'CBN'
                else:
                    LABEL = 'XM'
            else:
                # Extract flare labels from the file name
                tag = filename.split('@')[0]
                tag = tag.split('/')[-1]
                LABEL = tag.split('_ar')[0]

            # Read the file and extract necessary features
            df = pd.read_csv(filename, index_col=None, header=0, sep='\t')
            df = self.preprocessor(df)

            # Only USFLUX and R_VALUE is used
            USFLUX = df['USFLUX']
            R_VALUE = df['R_VALUE']

            li.append([USFLUX, R_VALUE, LABEL])
        # Create and return the dataframe build on the extracted features
        partition_frame = pd.DataFrame(li, columns=['USFLUX', 'R_VALUE', 'LABEL'])
        return partition_frame, count


    # This function converts multiple labels into binary labels
    # by defining X, M as flaring class while C, B, N as non-flaring class
    def convert_to_binary_class(self, label_col):
        for i in range(len(label_col)):
            if (label_col[i].startswith('X')) or (label_col[i].startswith('M')):
                label_col[i] = 'XM'
            else:
                label_col[i] = 'CBN'
        return label_col


    # This function build a Time Series Forest Classifier and generate necessary measurements on the classification results
    def fit(self, trainDF, count_list):
        # Extract training data/labels and testing data/labels
        trainDF_data = trainDF.loc[:, trainDF.columns != 'LABEL']
        trainDF_labels = trainDF['LABEL']

        # Have a glance of the training and testing dimensions
        print('# Model: TSF,,,')
        #print('Training Dimensions: ' + str(trainDF_data.shape))
        #print('Testing Dimensions: ' + str(testDF_data.shape))
        #print()

        self.clf.fit(trainDF_data, trainDF_labels)
        print('# Model Parameters: **' + str(self.clf).replace(',', ';').replace('\n', '').replace(' ', '') + '**,,,')
        print('# TrainingSet: **/data/SHARPS/BERKAY/v0.7/CLUS/partition1 & partition2** , number of positives(CBN): '
              + str(count_list[0]) + ', number of negatives(XM): ' + str(count_list[1]) + ',')
        print(
            '# TestingSet: **/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/partition4** , number of positives(CBN): '
            + str(count_list[2]) + ', number of negatives(XM): ' + str(count_list[3]) + ',')
        print('# ModelPath: **/home/aji1/SHARPS/OperationalTest/us_p1_p2**,,,')

        # Check if it is binary model
        if self.isBinary:
            name = '_binary'
        else:
            name = ''

        # Save the training model
        joblib.dump(self.clf, 'TSF' + name + '.pkl')

    # Predict label based on testing data
    def predict(self, testDF):
        testDF_data = testDF.loc[:, testDF.columns != 'LABEL']
        testDF_labels = testDF['LABEL']
        return testDF_labels, self.clf.predict(testDF_data)


    def TSS(self, table):
        """
        Calculates the true skill score based on the true classes and the predicted ones.

        Note: Keep in mind that the order of the class labels 'labels' in
        'confusion_matrix' defines the positive and negative classes. Here we set it to
        ["CBN", "XM"].

        (From Bobra's paper) - The flaring ARs correctly predicted as flaring are called true
        positives (TP), the flaring ARs incorrectly predicted as non-flaring are false negatives (FN),
        the non-flaring ARs correctly predicted as non-flaring are true negatives (TN), and the
        non-flaring ARs incorrectly predicted as flaring are false positives (FP). From these four
        quantities, various metrics are computed.
        """
        # this order is in line with the confusion_matrix function we use here.
        TN, FP, FN, TP = table

        tp_rate = TP / float(TP + FN) if TP > 0 else 0  # also known as "recall"
        fp_rate = FP / float(FP + TN) if FP > 0 else 0
        return tp_rate - fp_rate

    def HSS1(self, table):
        """
            Calculates the Heidke skill score based on the output of the confusion table function
            This is the first way to compute - using the Barnes & Leka (2008) definition
        """
        TN, FP, FN, TP = table
        N = TN + FP
        P = TP + FN
        HSS = (TP + TN - N) / float(P)
        return HSS

    def HSS2(self, table):
        """
            Calculates the Heidke skill score based on the output of the confusion table function
            This is the second way to compute - using the Mason & Hoeksema (2010) definition
        """
        TN, FP, FN, TP = table
        N = TN + FP
        P = TP + FN
        HSS = (2 * (TP * TN - FN * FP)) / float((P * (FN + TN) + (TP + FP) * N))
        return HSS

    def GSS(self, table):
        # this order is in line with the confusion_matrix function we use here.
        TN, FP, FN, TP = table

        CH = ((TP + FP) * (TP + FN)) / (TP + FP + FN + TN)
        GSS = (TP - CH) / (TP + FP + FN - CH)
        return GSS

    def precisionPos(self, table):
        TN, FP, FN, TP = table
        precisionPos = TP / float(TP + FP)
        return precisionPos

    def TPR(self, table):
        TN, FP, FN, TP = table
        TPR = TP / float(TP + FN)
        return TPR

    def precisionNeg(self, table):
        TN, FP, FN, TP = table
        precisionNeg = TN / float(TN + FN)
        return precisionNeg

    def TNR(self, table):
        TN, FP, FN, TP = table
        TNR = TN / float(TN + FP)
        return TNR

    def FAR(self, table):
        TN, FP, FN, TP = table
        FAR = FP / float(TP + FP)
        return FAR

    def POFD(self, table):
        TN, FP, FN, TP = table
        POFD = FP / float(TN + FP)
        return POFD

    def F1Pos(self, table):
        TN, FP, FN, TP = table
        precision = TP / float(TP + FP)
        recall = TP / float(TP + FN)
        f1 = 2 * ((precision * recall) / (precision + recall))
        return f1

    def F1Neg(self, table):
        TN, FP, FN, TP = table
        precision = TN / float(TN + FN)
        recall = TN / float(TN + FP)
        f1 = 2 * ((precision * recall) / (precision + recall))
        return f1

    def evaluation(self, testDF_labels, pred_labels):
        # Convert multiple labels into binary labels
        if self.isBinary:
            testDF_labels = self.convert_to_binary_class(testDF_labels)
            pred_labels = self.convert_to_binary_class(pred_labels)
        # Generate a confusion matrix based on given and predicted labels
        scores = confusion_matrix(testDF_labels, pred_labels, labels=["CBN", "XM"]).ravel()
        tn, fp, fn, tp = scores
        print('# Confusion Matrix,,,')
        print('#,,Predicted,')
        print('#,,XM,CBN')
        print('#Actual,XM,{0},{1}'.format(tp, fn))
        print('#,CBN,{0},{1}'.format(fp, tn))
        print('#,,,')
        print('Metric,Score,,')

        results_DF = pd.DataFrame(columns=['TSS', 'HSS', 'GSS', 'XMPr', 'TPR', 'CBNPr', 'TNR', 'FAR', 'POFD', 'f1XM', 'f1CBN'], index=[0])

        # Accuracy
        #scoreTrain = clf.score(trainDF_data, trainDF_labels)
        #scoreTest = clf.score(testDF_data, testDF_labels)
        #print("Train Accuracy: " + str(round(scoreTrain, 4)))
        #print("Test Accuracy: " + str(round(scoreTest, 4)))
        #results_DF['Accur'] = scoreTest

        # TSS
        tss = self.TSS(scores)
        print('TSS,{0},,'.format(tss))
        results_DF['TSS'] = tss

        # HSS2 Definition 2
        hss2 = self.HSS2(scores)
        print('HSS2,{0},,'.format(hss2))
        results_DF['HSS'] = hss2

        # GSS
        gss = self.GSS(scores)
        print('GSS,{0},,'.format(gss))
        results_DF['GSS'] = gss

        # Precision Positive
        posPrecision = self.precisionPos(scores)
        print('Precision(XM),{0},,'.format(posPrecision))
        results_DF['XMPr'] = posPrecision

        # TPR
        tpr = self.TPR(scores)
        print('TPR/Recall(XM),{0},,'.format(tpr))
        results_DF['TPR'] = tpr

        # Precision Negative
        negPrecision = self.precisionNeg(scores)
        print('Precision(CBN),{0},,'.format(negPrecision))
        results_DF['CBNPr'] = negPrecision

        # TNR
        tnr = self.TNR(scores)
        print('TNR/Recall(CBN),{0},,'.format(tnr))
        results_DF['TNR'] = tnr

        # FAR
        far = self.FAR(scores)
        print('FAR/FPR,{0},,'.format(far))
        results_DF['FAR'] = far

        # POFD
        pofd = self.POFD(scores)
        print('POFD/FDR,{0},,'.format(pofd))
        results_DF['POFD'] = pofd

        # F1(XM)
        f1XM = self.F1Pos(scores)
        print('F1(XM),{0},,'.format(f1XM))
        results_DF['f1XM'] = f1XM

        # F1(CBN)
        f1CBN = self.F1Neg(scores)
        print('F1(CBN),{0},,'.format(f1CBN))
        results_DF['f1CBN'] = f1CBN

        if self.isBinary:
            name = 'binary_'
        else:
            name = 'multi_'
        # Return the result measurement dataframe
        results_DF.to_csv("/home/aji1/SHARPS/DeployModel/" + name + "results.csv")#(constants.RESULT_PATH)
        return results_DF


def main():
    basepath = '/data/SHARPS/BERKAY/v0.7/CLUS/'
    testpath = '/data/SHARPS/BERKAY/v0.7/new-data-folds/instances_O12L0P24/'

    tsf = TSF()
    train_partition = ['partition1', 'partition2']
    test_partition = ['partition4']
    train, test, count = tsf.frame(basepath, testpath, train_partition, test_partition)
    tsf.fit(train, count)
    test_labels, pred_labels = tsf.predict(test)
    tsf.evaluation(test_labels, pred_labels)

    tsf2 = TSF(isBinary=False)
    train, test, count = tsf2.frame(basepath, testpath, train_partition, test_partition)
    tsf2.fit(train, count)
    test_labels, pred_labels = tsf2.predict(test)
    dataset = pd.DataFrame()
    dataset['Estimated Label'] = pred_labels.tolist()
    dataset['Actual Label'] = test_labels.tolist()
    dataset.to_csv("/home/aji1/SHARPS/DeployModel/multi_results.csv")
    #tsf2.evaluation(test_labels, pred_labels)



if __name__ == '__main__':
    main()





