# README #

This README would provide steps that are necessary to get the predictor up and running.


### Dataset ###

* An undersampled dataset called CLUS based on the SDO-era active region datasets (from May 2010 to August 2018). 
* The details of climatology-based undersampling (CLUS) method and a case study was provided in our earlier work in Ahmadzadeh et al. (2019).

### How do I get set up? ###

* The required libraries and packages are specified under __requirement.txt__
* All the pre-saved models are accessible under folder __trained-model__
* All the source code for training these models are under folder __src__
* All the results from training these models are under folder __results__

